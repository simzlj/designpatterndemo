designpatterndemo is an open-source project about design pattern examples.

It contains two parts:<br/>
part1: most examples in the book 《大话设计模式》, and UML diagrams;<br/>
part2: a model task schedule framework, it will be separaeted to be a <br/>
independent project in the future. The framework executes R task(R language) <br/>
or SQL task with multiply threads. It is a light framework and will be a <br/>
stable and high performance framework. It will support DAG tasks like Spark DAG.<br/>
Comparing with Spark, while executing DAG tasks, it handles them all in one node <br/>
with multiply threads. It is not designed to handle big data.<br/>

To get started using designpatterndemo, the full documentation for this release can be<br/>
found under the doc/ directory. Now documents are not enough, they are under developed.<br/>

The latest stable designpatterndemo can be downloaded from [1].

designpatterndemo is made available under the Apache License, version 2.0 [2]

1. https://gitee.com/simzlj/designpatterndemo/releases
2. http://hbase.apache.org/license.html