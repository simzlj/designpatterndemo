package test.com.xy6.model.thread;

import org.junit.Test;

import com.xy6.model.api.vo.MyJob;
import com.xy6.model.thread.MyJobThread;
import com.xy6.model.utils.MySystem;

/**
 * @author zhang
 * @since 2018-06-10
 */
public class MyJobThreadTest {

	@Test
	public void testNormal() throws InterruptedException{
		MySystem.println(String.format("cur thread name: %s", Thread.currentThread().getName()));
		MyJobThread thread = new MyJobThread(make());
		Thread t1 = new Thread(thread, "t1");
		MySystem.println(String.format("t1 name: %s", t1.getName()));
		t1.start();
		
		Thread.currentThread().join();
	}
	
	@Test
	public void testSuspendResume() throws InterruptedException{
		MySystem.println(String.format("cur thread name: %s", Thread.currentThread().getName()));
		MyJobThread thread = new MyJobThread(make());
		Thread t1 = new Thread(thread, "t1");
		MySystem.println(String.format("t1 name: %s", t1.getName()));
		t1.start();
		
		Thread.sleep(1000);
		thread.mysuspend();
		
		Thread.sleep(30000);
		thread.myresume();
		
		Thread.currentThread().join();
	}
	
	@Test
	public void testSuspendStop() throws InterruptedException{
		MySystem.println(String.format("cur thread name: %s", Thread.currentThread().getName()));
		MyJobThread thread = new MyJobThread(make());
		Thread t1 = new Thread(thread, "t1");
		MySystem.println(String.format("t1 name: %s", t1.getName()));
		t1.start();
		
		Thread.sleep(1000);
		thread.mysuspend();
		
		Thread.sleep(20000);
		thread.mystop();
		
		Thread.currentThread().join();
	}
	
	@Test
	public void testStop() throws InterruptedException{
		MySystem.println(String.format("cur thread name: %s", Thread.currentThread().getName()));
		MyJobThread thread = new MyJobThread(make());
		Thread t1 = new Thread(thread, "t1");
		MySystem.println(String.format("t1 name: %s", t1.getName()));
		t1.start();
		
		Thread.sleep(1000);
		thread.mystop();
		
		Thread.currentThread().join();
	}
	
	private static MyJob make() {
		MyJob job = new MyJob();
		job.setId("j001");
		job.setCron("* * * * * ?");
		job.setTaskIds("t001,t002,t003,t004");
		job.setLines("t001,t002;t001,t003;t002,t004;t003,t004");

		return job;
	}
	
}
