package test.com.xy6.model.thread;

import org.junit.Test;

import com.xy6.model.api.vo.MyJob;
import com.xy6.model.bean.wrapper.MyJobWrapper;
import com.xy6.model.thread.MyJobExec;

/**
 * @author zhang
 * @since 
 */
public class MyJobExecTest {

	/**
	 * TODO 测试不通过
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testPostExecDAG1() throws InterruptedException {
		MyJob job = make();
		MyJobWrapper jobWrapper = new MyJobWrapper(job);
		MyJobExec exec = new MyJobExec(jobWrapper);
		exec.exec();
		
		Thread.currentThread().join();
	}
	
	private static MyJob make(){
		MyJob job = new MyJob();
		job.setId("j001");
		job.setCron("* * * * * ?");
		job.setTaskIds("t001,t002,t003,t004");
		job.setLines("t001,t002;t001,t003;t002,t004;t003,t004");

		return job;
	}
	
}
