package test.com.xy6.model.service;

import org.junit.Test;

import com.xy6.model.api.service.IMyExecutorService;
import com.xy6.model.service.MyExecutorService;

/**
 * @author zhang
 * @since 
 */
public class MyExecutorServiceTest {

	private final static String jobId = "job001";
	
	@Test
	public void testNormal() throws InterruptedException {
		IMyExecutorService service = new MyExecutorService();
		service.submit(jobId);
		
		Thread.currentThread().join();
	}
	
	@Test
	public void testSuspendResume() throws InterruptedException {
		IMyExecutorService service = new MyExecutorService();
		System.out.println(service.submit(jobId));
		
		Thread.sleep(1000);
		System.out.println(service.suspend(jobId));
		
		Thread.sleep(20000);
		System.out.println(service.resume(jobId));
		
		Thread.currentThread().join();
	}
	
	@Test
	public void testSuspendStop() throws InterruptedException {
		IMyExecutorService service = new MyExecutorService();
		System.out.println(service.submit(jobId));
		
		Thread.sleep(1000);
		System.out.println(service.suspend(jobId));
		
		Thread.sleep(20000);
		System.out.println(service.stop(jobId));
		
		Thread.currentThread().join();
	}
	
	@Test
	public void testSuspendForceStop() throws InterruptedException {
		IMyExecutorService service = new MyExecutorService();
		System.out.println(service.submit(jobId));
		
		Thread.sleep(1000);
		System.out.println(service.suspend(jobId));
		
		Thread.sleep(20000);
		System.out.println(service.forceStop(jobId));
		
		Thread.currentThread().join();
	}

}
