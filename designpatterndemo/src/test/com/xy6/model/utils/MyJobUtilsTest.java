package test.com.xy6.model.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.xy6.model.api.vo.MyJob;
import com.xy6.model.utils.MyJobUtils;

/**
 * @author zhang
 * @since 
 */
public class MyJobUtilsTest {

	@Test
	public void testVerify1() {
		MyJob job = new MyJob();
		job.setId("job001");
		job.setCron("* * * * * ?");
		job.setTaskIds("task001,task002,task003");
		job.setLines("task001,task002;task001,task003");
		assertEquals(true, MyJobUtils.verify(job));
	}

}
