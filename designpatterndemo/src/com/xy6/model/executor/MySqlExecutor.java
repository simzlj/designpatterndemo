package com.xy6.model.executor;

import com.xy6.model.bean.wrapper.MyTaskWrapper;
import com.xy6.model.executor.handler.MyHandler;
import com.xy6.model.executor.handler.MyInputHandler;
import com.xy6.model.executor.handler.MySqlExecHandler;
import com.xy6.model.utils.MySystem;

public class MySqlExecutor extends MyModelExecutor {

	@Override
	public void exec(MyTaskWrapper task) throws InterruptedException {
		MySystem.println("MySqlExecutor.exec");
		MyHandler handler = new MyInputHandler();
		handler.handle(task, new MySqlExecHandler());
	}

}
