package com.xy6.model.executor;

import com.xy6.model.bean.wrapper.MyTaskWrapper;
import com.xy6.model.executor.handler.MyHandler;
import com.xy6.model.executor.handler.MyInputHandler;
import com.xy6.model.executor.handler.MyRExecHandler;
import com.xy6.model.utils.MySystem;

/**
 * 执行一个R任务
 * 
 * @author zhang
 *
 */
public class MyRExecutor extends MyModelExecutor {

	@Override
	public void exec(MyTaskWrapper task) throws InterruptedException {
		MySystem.println("MyRExecutor.exec");
		MyHandler handler = new MyInputHandler();
		handler.handle(task, new MyRExecHandler());
	}

}
