package com.xy6.model.executor.handler;

import com.xy6.model.bean.MyInputBean;
import com.xy6.model.bean.MyOutputBean;
import com.xy6.model.bean.wrapper.MyTaskWrapper;

public class MySqlExecHandler extends MyHandler {
	
	@Override
	public void handle(MyTaskWrapper task, MyInputBean input) throws InterruptedException{
		// inputbean转换为outputbean
		MyOutputBean output = new MyOutputBean();
		
		MyHandler handler = new MyOutputHandler();
		handler.handle(task, output);
	}

}
