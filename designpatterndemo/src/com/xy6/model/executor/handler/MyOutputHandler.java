package com.xy6.model.executor.handler;

import com.xy6.model.bean.MyOutputBean;
import com.xy6.model.bean.wrapper.MyTaskWrapper;
import com.xy6.model.sql.MySqlUpdate;
import com.xy6.model.utils.MySystem;

public class MyOutputHandler extends MyHandler {
	
	@Override
	public void handle(MyTaskWrapper task, MyOutputBean output) throws InterruptedException {
		MySystem.println("MyOutputHandler.handle");
		// 保存输出数据
		MySqlUpdate update = new MySqlUpdate();
		update.save(output, task.getSaveSql().split(";"));
	}
	
}
