package com.xy6.model.executor.handler;

import com.xy6.model.bean.MyInputBean;
import com.xy6.model.bean.wrapper.MyTaskWrapper;
import com.xy6.model.sql.MySqlRead;
import com.xy6.model.utils.MySystem;

public class MyInputHandler extends MyHandler {

	@Override
	public void handle(MyTaskWrapper task, MyHandler handler) throws InterruptedException{
		MySystem.println("MyInputHandler.handle");
		// 执行sql，获取输入数据，进行格式化
		MySqlRead read = new MySqlRead();
		MyInputBean input = read.query(task.getQuerySql().split(";"));
		
		handler.handle(task, input);
	}
	
}
