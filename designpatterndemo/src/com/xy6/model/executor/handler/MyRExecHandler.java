package com.xy6.model.executor.handler;

import com.xy6.model.bean.MyInputBean;
import com.xy6.model.bean.MyOutputBean;
import com.xy6.model.bean.wrapper.MyModelWrapper;
import com.xy6.model.bean.wrapper.MyTaskWrapper;
import com.xy6.model.utils.MySystem;
import com.xy6.model.utils.RConnection;
import com.xy6.model.utils.RConnectionUtils;

public class MyRExecHandler extends MyHandler {
	
	@Override
	public void handle(MyTaskWrapper task, MyInputBean input) throws InterruptedException {
		MySystem.println("MyRExecHandler.handle");
		MyModelWrapper model = task.getModelInfo();
		// 获取一个Rserve连接
		RConnection conn = RConnectionUtils.getConn();
		// 加载R文件
		conn.source(model.getFiles());
		// 调用入口函数，执行模型
		conn.eval("x <- input");
		conn.eval("y <- 100");
		MyOutputBean output = conn.eval(model.getEntrance() + "(x, y)");
		conn.close();
		// 3-13s
//		long mills = Double.valueOf(Math.random() * 10 + 3).intValue() * 1000;
		long mills = 3000;
		MySystem.println(String.format("sleep %d ms", mills));
		Thread.sleep(mills);
		
		MyHandler handler = new MyOutputHandler();
		handler.handle(task, output);
	}

}
