package com.xy6.model.executor.handler;

import com.xy6.model.bean.MyInputBean;
import com.xy6.model.bean.MyOutputBean;
import com.xy6.model.bean.wrapper.MyTaskWrapper;

public abstract class MyHandler {

	public void handle(MyTaskWrapper task, MyHandler handler) throws InterruptedException {
	}

	public void handle(MyTaskWrapper task, MyInputBean input) throws InterruptedException {
	}

	public void handle(MyTaskWrapper task, MyOutputBean input) throws InterruptedException {
	}

}
