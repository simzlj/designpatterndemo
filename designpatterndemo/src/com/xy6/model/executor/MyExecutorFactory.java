package com.xy6.model.executor;

import com.xy6.model.api.enums.MyModelType;

public class MyExecutorFactory {

	public static MyModelExecutor produce(String type) {
		if (MyModelType.R.getType().equals(type)) {
			return new MyRExecutor();
		} else if (MyModelType.SQL.getType().equals(type)) {
			return new MySqlExecutor();
		}
		return null;
	}

}
