package com.xy6.model.executor;

import com.xy6.model.bean.wrapper.MyTaskWrapper;

public abstract class MyModelExecutor {

	public abstract void exec(MyTaskWrapper task) throws InterruptedException;
	
}
