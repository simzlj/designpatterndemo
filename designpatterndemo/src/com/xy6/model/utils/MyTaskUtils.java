package com.xy6.model.utils;

import java.util.ArrayList;
import java.util.List;

import com.xy6.model.api.vo.MyTask;
import com.xy6.model.bean.wrapper.MyTaskWrapper;

public class MyTaskUtils {

	public static List<MyTaskWrapper> findTasks(String taskIds) {
		String[] ids = taskIds.split(",");
		int length = ids.length;
		List<MyTaskWrapper> tasks = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			tasks.add(new MyTaskWrapper(make(ids[i])));
		}

		return tasks;
	}

	private static MyTask make() {
		MyTask task = new MyTask();
		task.setId("task001");
		task.setModelId("model001");
		task.setQuerySql("select yhbh from kh_ydkh");
		task.setSaveSql("delete from user where yhbh = {yhbh};");

		return task;
	}
	
	private static MyTask make(String id) {
		MyTask task = new MyTask();
		task.setId(id);
		task.setModelId("model001");
		task.setQuerySql("select yhbh from kh_ydkh");
		task.setSaveSql("delete from user where yhbh = {yhbh};");
		
		return task;
	}

}
