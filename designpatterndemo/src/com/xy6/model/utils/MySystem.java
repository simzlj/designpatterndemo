package com.xy6.model.utils;

/**
 * @author zhang
 * @since 
 */
public class MySystem {

	private MySystem(){
	}

	public static void println(String str){
		System.out.println(String.format("%s, %s", Thread.currentThread().getName(), str));
	}
	
}
