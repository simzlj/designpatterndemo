package com.xy6.model.utils;

import java.util.ArrayList;
import java.util.List;

import com.xy6.model.api.enums.MyModelType;
import com.xy6.model.api.vo.MyModel;
import com.xy6.model.bean.wrapper.MyModelWrapper;

public class MyModelUtils {

	public static List<String> findFiles(String modelId) {
		return makeFiles();
	}
	
	public static MyModelWrapper findInfo(String modelId){
		return new MyModelWrapper(makeRModel());
	}
	
	public static MyModel makeRModel(){
		MyModel model = new MyModel();
		model.setId("model001");
		model.setName("R模型测试1");
		model.setType(MyModelType.R.getType());
		model.setEntrance("main");
		
		return model;
	}
	
	public static MyModel makeSqlModel(){
		MyModel model = new MyModel();
		model.setId("model001");
		model.setName("R模型测试1");
		model.setType(MyModelType.SQL.getType());
		model.setEntrance("main");
		
		return model;
	}
	
	private static List<String> makeFiles(){
		List<String> files = new ArrayList<>(2);
		files.add("/usr/local/1.R");
		files.add("/usr/local/2.R");
		
		return files;
	}

}
