package com.xy6.model.api.bean;

import java.util.Date;

import com.xy6.model.api.enums.MyStatus;

public class MyStatusWrapper {

	private MyStatus status;
	
	private Date date;

	public MyStatusWrapper(){
	}
	
	public MyStatusWrapper(MyStatus status, Date date){
		this.status = status;
		this.date = date;
	}
	
	public MyStatus getStatus() {
		return status;
	}
	
	public String getName() {
		return status.getName();
	}

	public void setStatus(MyStatus status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "MyJobStatusInfo [status=" + status + ", date=" + date + "]";
	}
	
}
