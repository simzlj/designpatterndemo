package com.xy6.model.api.vo;

/**
 * 文件信息
 * 
 * @author zhang
 * @since 2018-05-06
 */
public class MyFile {

	/** 编号 */
	private String id;
	
	/** 文件全路径，绝对路径 */
	private String path;
	
	/** 名称，如1.txt */
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "MyFile [id=" + id + ", path=" + path + ", name=" + name + "]";
	}
	
}
