package com.xy6.model.api.vo;

/**
 * 模型。是一个独立的数据分析模型，有模型文件、模型调用入口、模型类型
 * 
 * @author zhang
 * @since 2018-05-06
 */
public class MyModel {

	/** 编号，唯一标识的字符串 */
	private String id;
	
	/** 名称 */
	private String name;
	
	/** 类型，r或sql */
	private String type;
	
	/** 模型入口函数名称 */
	private String entrance;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEntrance() {
		return entrance;
	}

	public void setEntrance(String entrance) {
		this.entrance = entrance;
	}
	
	@Override
	public String toString() {
		return "MyModel [id=" + id + ", name=" + name + ", type=" + type + ", entrance=" + entrance + "]";
	}

}
