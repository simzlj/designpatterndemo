package com.xy6.model.api.vo;

/**
 * 模型文件关系，一个模型包含0个或多个文件信息
 * 
 * @author zhang
 * @since 2018-05-06
 */
public class MyModelFile {

	/** 模型编号 */
	private String modelId;
	
	/** 文件编号 */
	private String fileId;

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	@Override
	public String toString() {
		return "MyModelFile [modelId=" + modelId + ", fileId=" + fileId + "]";
	}
	
}
