package com.xy6.model.api.vo;

/**
 * 任务，执行任务的最小单元
 * 
 * @author zhang
 * @since 2018-05-06
 */
public class MyTask {

	private String id;
	
	private String modelId;
	
	private String querySql;
	
	private String saveSql;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getQuerySql() {
		return querySql;
	}

	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}

	public String getSaveSql() {
		return saveSql;
	}

	public void setSaveSql(String saveSql) {
		this.saveSql = saveSql;
	}

	@Override
	public String toString() {
		return "MyTask [id=" + id + ", modelId=" + modelId + ", querySql=" + querySql + ", saveSql=" + saveSql + "]";
	}

}
