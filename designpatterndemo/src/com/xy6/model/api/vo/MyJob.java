package com.xy6.model.api.vo;


/**
 * 作业，一个作业可以包含一个或多个任务，多个任务构成一个有向无环图
 * 
 * @author zhang
 * @since 2018-05-06
 */
public class MyJob {

	private String id;
	
	/** 任务编号，以英文逗号分隔。dag中的顶点 */
	private String taskIds;
	
	/** dag中的边，以分号分隔，每个边有两个顶点，以逗号分隔。如001,002;001,003 */
	private String lines;
	
	/** cron表达式 */
	private String cron;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public String getTaskIds() {
		return taskIds;
	}

	public void setTaskIds(String taskIds) {
		this.taskIds = taskIds;
	}
	
	public String getLines() {
		return lines;
	}

	public void setLines(String lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		return "MyJob [id=" + id + ", taskIds=" + taskIds + ", lines=" + lines + ", cron=" + cron + "]";
	}

}
