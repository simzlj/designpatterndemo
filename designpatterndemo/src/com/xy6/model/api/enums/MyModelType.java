package com.xy6.model.api.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 模型类型枚举
 * 
 * @author zhang
 * @since 2018-05-06
 */
public enum MyModelType {

	R("R", "R模型，包含一个或多个R文件"),

	SQL("SQL", "SQL模型，包含一个或多个sql语句"),

	JAVA("JAVA", "JAVA模型");

	/** 类型 */
	private String type;

	/** 描述信息 */
	private String message;

	/** 保存所有枚举类型 */
	private static Map<String, MyModelType> map;

	private MyModelType(String type, String message) {
		this.type = type;
		this.message = message;
		initMap(type, this);
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

	/**
	 * 初始化枚举类型集合
	 * 
	 * @param type
	 * @param taskType
	 */
	private static void initMap(String type, MyModelType taskType) {
		if (null == map) {
			map = new HashMap<String, MyModelType>(5);
		}
		map.put(type, taskType);
	}

	public static MyModelType getModelType(String type) {
		return map.get(type);
	}

	public static Map<String, MyModelType> getMap() {
		return map;
	}

}
