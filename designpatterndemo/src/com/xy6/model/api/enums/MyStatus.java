package com.xy6.model.api.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 作业状态枚举
 * 
 * @author zhang
 * @since 2018-05-06
 */
public enum MyStatus {
	
	CREATE("CREATE", "创建"), 
	
	BEGIN("BEGIN", "开始执行"), 
	
	INTERRUPT("INTERRUPT", "中断"), 
	
	END("END", "执行结束");

	/** 名称 */
	private String name;

	/** 描述信息 */
	private String message;

	/** 保存所有枚举类型 */
	private static Map<String, MyStatus> map;

	private MyStatus(String name, String message) {
		this.name = name;
		this.message = message;
		initMap(name, this);
	}

	public String getName() {
		return name;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return name.toString();
	}

	/**
	 * 初始化枚举类型集合
	 * 
	 * @param status
	 * @param taskType
	 */
	private void initMap(String name, MyStatus jobStatus) {
		if(null == map){
			map = new HashMap<String, MyStatus>(5);
		}
		map.put(name, jobStatus);
	}

	public static MyStatus getJobStatus(String name) {
		return map.get(name);
	}

	public static Map<String, MyStatus> getMap() {
		return map;
	}

}
