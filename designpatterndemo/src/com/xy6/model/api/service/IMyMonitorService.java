package com.xy6.model.api.service;

import com.xy6.model.api.bean.MyStatusWrapper;

/**
 * 任务执行监控接口，查询job执行进度。
 * 客户端一般通过心跳检测，监控job执行
 * 
 * @author zhang
 *
 */
public interface IMyMonitorService {
	
	public MyStatusWrapper track(String jobId);

}
