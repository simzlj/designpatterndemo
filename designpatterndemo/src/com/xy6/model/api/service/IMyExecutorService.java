package com.xy6.model.api.service;

/**
 * 执行任务接口。客户端通过该接口提交一个job
 * 
 * @author zhang
 *
 */
public interface IMyExecutorService {

	public String submit(String jobId);

	/**
	 * 暂停执行。如果任务正在执行，则会等待任务执行结束，再暂停
	 * 返回为空字符串表示执行成功；返回不为空字符串，表示执行失败，字符串内容为失败信息。
	 * 
	 * @param jobId
	 * @return
	 */
	public String suspend(String jobId);
	
	/**
	 * 恢复执行
	 * 
	 * @param jobId
	 * @return
	 */
	public String resume(String jobId);
	
	/**
	 * 停止执行。如果任务正在执行，则会等待任务执行结束，再停止
	 * 
	 * @param jobId
	 * @return
	 */
	public String stop(String jobId);
	
	/**
	 * 强制停止执行。不管是否有任务在执行，直接停止。
	 * 
	 * @param jobId
	 * @return
	 */
	public String forceStop(String jobId);
	
}
