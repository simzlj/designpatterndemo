package com.xy6.model.sql;

import java.util.List;

import com.xy6.model.bean.MyInputBean;

/**
 * 执行查询相关的脚本
 * 
 * @author zhang
 *
 */
public class MySqlRead extends MyAbstractSqlRunner {

	@Override
	public MyInputBean query(String sql) {
		return new MyInputBean();
	}

	@Override
	public MyInputBean query(List<String> sqls) {
		return new MyInputBean();
	}

}
