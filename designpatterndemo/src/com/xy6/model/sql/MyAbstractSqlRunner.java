package com.xy6.model.sql;

import java.util.Arrays;
import java.util.List;

import com.xy6.model.bean.MyInputBean;
import com.xy6.model.bean.MyOutputBean;

public class MyAbstractSqlRunner {

	public MyInputBean query(String sql){
		return null;
	}
	
	public MyInputBean query(List<String> sqls){
		return null;
	}
	
	public MyInputBean query(String[] sqls){
		return query(Arrays.asList(sqls));
	}
	
	public boolean save(MyOutputBean obj, List<String> sqls){
		return true;
	}
	
	public boolean save(MyOutputBean obj, String sql){
		return true;
	}
	
	public boolean save(MyOutputBean obj, String[] sqls){
		return save(obj, Arrays.asList(sqls));
	}
}
