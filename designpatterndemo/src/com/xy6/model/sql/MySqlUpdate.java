package com.xy6.model.sql;

import java.util.List;

import com.xy6.model.bean.MyOutputBean;

/**
 * 执行修改、删除相关的脚本
 * 
 * @author zhang
 *
 */
public class MySqlUpdate extends MyAbstractSqlRunner {

	@Override
	public boolean save(MyOutputBean obj, List<String> sqls) {
		return super.save(obj, sqls);
	}

	@Override
	public boolean save(MyOutputBean obj, String sql) {
		return super.save(obj, sql);
	}

}
