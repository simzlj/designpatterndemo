package com.xy6.model.thread;

import com.xy6.model.utils.MySystem;

public abstract class MyAbstractExecTemplate {

	public void exec(){
		MySystem.println("MyAbstractExecTemplate.exec()");
		beforeExec();
		postExec();
		afterExec();
	}
	
	protected void beforeExec(){
	}
	
	protected void postExec(){
	}
	
	protected void afterExec(){
	}
	
	/**
	 * 修改标识位，挂起当前线程
	 */
	public void mysuspend(){
	}
	
	/**
	 * 恢复线程，继续执行
	 */
	public void myresume(){
	}
	
	/**
	 * 停止任务。会等待运行中的任务执行结束
	 */
	public void mystop(){
	}
	
}
