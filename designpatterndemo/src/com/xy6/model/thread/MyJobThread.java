package com.xy6.model.thread;

import com.xy6.model.api.enums.MyStatus;
import com.xy6.model.api.vo.MyJob;
import com.xy6.model.bean.wrapper.MyJobWrapper;
import com.xy6.model.service.manager.MyJobManager;
import com.xy6.model.service.monitor.MyThreadMonitor;
import com.xy6.model.utils.MySystem;

/**
 * 执行一个作业
 * 
 * @author zhang
 *
 */
public class MyJobThread implements Runnable {

	private MyJob job;

	private MyJobWrapper jobWrapper;
	
	private MyAbstractExecTemplate jobExec;

	public MyJobThread(MyJob job) {
		this.job = job;
	}

	@Override
	public void run() {
		MySystem.println("MyJobThread.run(), " + Thread.currentThread().getName());
		// 监控线程
		MyThreadMonitor.put(job.getId(), this);
		this.jobWrapper = new MyJobWrapper(job);
		// 记录job执行进度
		MyJobManager.updateStatus(jobWrapper, MyStatus.CREATE);

		jobExec = new MyJobExec(jobWrapper);
		jobExec.exec();
	}
	
	public void mysuspend(){
		MySystem.println("MyJobThread.mysuspend()");
		jobExec.mysuspend();
	}
	
	/**
	 * 恢复线程，继续执行
	 */
	public void myresume(){
		MySystem.println("MyJobThread.myresume()");
		jobExec.myresume();
	}
	
	/**
	 * 停止任务
	 */
	public void mystop(){
		MySystem.println("MyJobThread.mystop()");
		jobExec.mystop();
	}
	
}
