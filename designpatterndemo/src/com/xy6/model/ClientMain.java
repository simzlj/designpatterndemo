package com.xy6.model;

import com.xy6.model.api.service.IMyExecutorService;
import com.xy6.model.service.MyExecutorService;
import com.xy6.model.utils.MySystem;

/**
 * 模拟客户端调用
 * 
 * @author zhang
 * @since 2018-05-18
 */
public class ClientMain {

	public static void main(String[] args) {
		// 触发一个作业的定时器，开始执行一个作业
		// 从线程池获取一个线程，执行这个作业
		IMyExecutorService service = new MyExecutorService();
		service.submit("job001");
		// 执行前后，更新执行进度，将进度保存到一个变量中
		// 查询作业包含的任务列表，逐个执行
		// 获取任务的类型，使用匹配的实现类执行任务
		// 执行前后，更新执行进度，将进度保存到一个变量中
		// 执行完一个任务后，再执行下一个任务
		MySystem.println("hello world");
	}
	
}
