package com.xy6.model.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author zhang
 * @since 
 */
public class MyThreadPool {

	private static ExecutorService executor = Executors.newCachedThreadPool();

	private MyThreadPool(){
	}
	
    public static Future<?> submit(Runnable task){
    	return executor.submit(task);
    }
	
}
