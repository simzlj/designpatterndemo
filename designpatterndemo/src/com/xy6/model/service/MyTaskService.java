package com.xy6.model.service;

import java.util.ArrayList;
import java.util.List;

import com.xy6.model.api.vo.MyTask;

public class MyTaskService {

	/**
	 * 根据多个任务编号，查询多个任务
	 * 
	 * @param taskIds
	 * @return
	 */
	public List<MyTask> findTasks(String taskIds){
		return new ArrayList<MyTask>(0);
	}
	
	public MyTask findTask(String id){
		return new MyTask();
	}
	
}
