package com.xy6.model.service.monitor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.xy6.model.thread.MyJobThread;

public class MyThreadMonitor {

	/**
	 * 保存运行中线程状态，多线程共享
	 */
	private static Map<String, MyJobThread> threads = new ConcurrentHashMap<>(100);
	
	/**
	 * 避免实例化
	 */
	private MyThreadMonitor(){
	}
	
	public static void put(String jobId, MyJobThread status){
		threads.put(jobId, status);
	}
	
	public static MyJobThread get(String jobId){
		return threads.get(jobId);
	}
	
	public static void remove(String jobId){
		threads.remove(jobId);
	}
	
	public static boolean contains(String jobId){
		return threads.containsKey(jobId);
	}

}
