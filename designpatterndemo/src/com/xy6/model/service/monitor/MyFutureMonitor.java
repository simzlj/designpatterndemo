package com.xy6.model.service.monitor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

/**
 * @author zhang
 * @since 
 */
@SuppressWarnings("rawtypes")
public class MyFutureMonitor {

	/**
	 * 保存运行中线程状态，多线程共享
	 */
	private static Map<String, Future> futures = new ConcurrentHashMap<>(100);
	
	/**
	 * 避免实例化
	 */
	private MyFutureMonitor(){
	}
	
	public static void put(String jobId, Future status){
		futures.put(jobId, status);
	}
	
	public static Future get(String jobId){
		return futures.get(jobId);
	}
	
	public static void remove(String jobId){
		futures.remove(jobId);
	}

	public static boolean contains(String jobId){
		return futures.containsKey(jobId);
	}
	
}
