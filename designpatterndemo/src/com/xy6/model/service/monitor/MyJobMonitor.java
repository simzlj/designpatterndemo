package com.xy6.model.service.monitor;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.xy6.model.api.bean.MyStatusWrapper;
import com.xy6.model.api.enums.MyStatus;

/**
 * 作业跟踪器，保存所有在运行的作业的执行状态
 * 
 * @author zhang
 *
 */
public class MyJobMonitor {

	/**
	 * 保存运行中作业状态，多线程共享。key-jobid，value-status
	 */
	private static Map<String, MyStatusWrapper> jobs = new ConcurrentHashMap<>(100);
	
	/**
	 * 避免实例化
	 */
	private MyJobMonitor(){
	}
	
	public static void put(String jobId, MyStatusWrapper status){
		jobs.put(jobId, status);
	}
	
	public static MyStatusWrapper get(String jobId){
		return jobs.get(jobId);
	}
	
	public static void remove(String jobId){
		jobs.remove(jobId);
	}
	
	public static boolean contains(String jobId){
		return jobs.containsKey(jobId);
	}
	
	public static void print(){
		for(Entry<String, MyStatusWrapper> elem : jobs.entrySet()){
			System.err.println(elem);
		}
		System.err.println();
	}
	
	public static void putId(String jobId){
		jobs.put(jobId, new MyStatusWrapper(MyStatus.CREATE, new Date()));
	}
	
	public static boolean containsId(String jobId){
		return jobs.containsKey(jobId);
	}
	
}
