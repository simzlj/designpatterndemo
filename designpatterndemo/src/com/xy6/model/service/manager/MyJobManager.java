package com.xy6.model.service.manager;

import java.util.Date;

import com.xy6.model.api.bean.MyStatusWrapper;
import com.xy6.model.api.enums.MyStatus;
import com.xy6.model.bean.wrapper.MyJobWrapper;
import com.xy6.model.service.monitor.MyJobMonitor;
import com.xy6.model.utils.MySystem;

/**
 * job管理器，管理job status等信息
 * 
 * @author zhang
 *
 */
public class MyJobManager {

	private MyJobManager() {
	}

	public static void updateStatus(MyJobWrapper job, MyStatus status) {
		// 记录日志
		MySystem.println(String.format("MyJobManager.updateStatus job %s %s", job.getId(), status.toString()));
		if (MyJobMonitor.contains(job.getId())) {
			// job已存在，更新执行进度
			MyStatusWrapper jobStatus = MyJobMonitor.get(job.getId());
			jobStatus.setStatus(status);
			jobStatus.setDate(new Date());

			// 如果job结束，则移除记录
		} else {
			// job不存在
			Date date = new Date();
			MyStatusWrapper jobStatus = new MyStatusWrapper(status, date);
			MyJobMonitor.put(job.getId(), jobStatus);
		}
	}

}
