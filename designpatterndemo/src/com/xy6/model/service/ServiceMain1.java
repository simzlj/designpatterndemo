package com.xy6.model.service;

import com.xy6.model.api.service.IMyExecutorService;
import com.xy6.model.service.monitor.MyJobMonitor;
import com.xy6.model.utils.MySystem;

public class ServiceMain1 {

	public static void main(String[] args) throws InterruptedException {
		testMultiJob3();
	}
	
	public static void testOneJob() throws InterruptedException {
		IMyExecutorService service = new MyExecutorService();
		service.submit("job001");
		MySystem.println("hello world");

		Thread.sleep(1000);
		MySystem.println(service.suspend("job001"));

		Thread.sleep(10000);
		MySystem.println(service.resume("job001"));

		Thread.sleep(3000);
		MySystem.println(service.stop("job001"));
	}
	
	public static void testMultiJob1() throws InterruptedException{
		Thread m1 = new Thread(new MyMonitor());
		m1.start();
		
		Thread t1 = new Thread(new MyThread("job001"));
		Thread t2 = new Thread(new MyThread("job001"));
		Thread t3 = new Thread(new MyThread("job001"));
		t1.start();
		t2.start();
		t3.start();
		
		Thread.currentThread().join();
	}
	
	public static void testMultiJob2() throws InterruptedException{
		Thread m1 = new Thread(new MyMonitor());
		m1.start();
		
		Thread t1 = new Thread(new MyThread("job001"));
		Thread t2 = new Thread(new MyThread("job002"));
		Thread t3 = new Thread(new MyThread("job003"));
		t1.start();
		t2.start();
		t3.start();
		
		Thread.currentThread().join();
	}
	
	public static void testMultiJob3() throws InterruptedException{
		Thread m1 = new Thread(new MyMonitor());
		m1.start();
		
		Thread t1 = new Thread(new MyThread("job001"));
		Thread t2 = new Thread(new MyThread("job002"));
		Thread t3 = new Thread(new MyThread("job003"));
		t1.start();
		t2.start();
		t3.start();
		
		Thread.sleep(4000);
		IMyExecutorService service = new MyExecutorService();
		service.stop("job002");
		
		Thread.currentThread().join();
	}
	
	private static class MyThread implements Runnable {
		private String jobId;
		public MyThread(String jobId){
			this.jobId = jobId;
		}
		@Override
		public void run(){
			IMyExecutorService service = new MyExecutorService();
			service.submit(jobId);
		}
	}

	private static class MyMonitor implements Runnable {
		@Override
		public void run(){
			for(int i=0; i<10; i++){
				MyJobMonitor.print();
				
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
				}
			}
		}
	}
	
	
}
