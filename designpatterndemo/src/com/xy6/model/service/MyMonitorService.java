package com.xy6.model.service;

import com.xy6.model.api.bean.MyStatusWrapper;
import com.xy6.model.api.service.IMyMonitorService;
import com.xy6.model.service.monitor.MyJobMonitor;

public class MyMonitorService implements IMyMonitorService {

	@Override
	public MyStatusWrapper track(String jobId) {
		return MyJobMonitor.get(jobId);
	}

}
