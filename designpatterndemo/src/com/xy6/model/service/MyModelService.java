package com.xy6.model.service;

import com.xy6.model.api.enums.MyModelType;
import com.xy6.model.api.vo.MyModel;

public class MyModelService {

	public MyModel find(String id){
		return make();
	}
	
	private static MyModel make(){
		MyModel model = new MyModel();
		model.setId("model001");
		model.setName("R模型测试1");
		model.setType(MyModelType.R.getType());
		model.setEntrance("main");
		
		return model;
	}

}
