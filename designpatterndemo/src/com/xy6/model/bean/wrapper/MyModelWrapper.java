package com.xy6.model.bean.wrapper;

import java.util.List;

import com.xy6.model.api.vo.MyModel;
import com.xy6.model.utils.MyModelUtils;

/**
 * model的包装类，包含model的全部信息
 * 
 * @author zhang
 * @since 2018-05-12
 */
public class MyModelWrapper {

	private MyModel model;
	
	/** 文件全路径集合 */
	private List<String> files;
	
	public MyModelWrapper(MyModel model){
		this.model = model;
		this.files = MyModelUtils.findFiles(model.getId());
	}

	public MyModel getModel() {
		return model;
	}

	public List<String> getFiles() {
		return files;
	}

	public String getId() {
		return model.getId();
	}

	public String getName() {
		return model.getName();
	}

	public String getType() {
		return model.getType();
	}

	public String getEntrance() {
		return model.getEntrance();
	}
		
}
