package com.xy6.model.bean.wrapper;

import com.xy6.model.api.vo.MyTask;
import com.xy6.model.utils.MyModelUtils;

/**
 * task的包装类，包含task的全部信息，后续执行时，无需再查询基本信息
 * 
 * @author zhang
 * @since 2018-05-12
 */
public class MyTaskWrapper {

	private MyTask task;
	
	private MyModelWrapper modelInfo;
	
	public MyTaskWrapper(MyTask task){
		this.task = task;
		this.modelInfo = MyModelUtils.findInfo(task.getModelId());
	}

	public MyModelWrapper getModelInfo() {
		return modelInfo;
	}

	public void setModelInfo(MyModelWrapper modelInfo) {
		this.modelInfo = modelInfo;
	}
	
	public String getId() {
		return this.task.getId();
	}

	public String getModelId() {
		return this.task.getModelId();
	}

	public String getQuerySql() {
		return this.task.getQuerySql();
	}

	public String getSaveSql() {
		return this.task.getSaveSql();
	}

}
