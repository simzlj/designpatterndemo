package com.xy6.dpdemo.book.chapter16.state;

/**
 * 傍晚工作状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateDusk implements IState {

	@Override
	public void handle(Work work) {
		if(work.isFinished()){
			new StateAfterWork().handle(work);
		} else {
			if(work.getHour() < 20){
				System.out.println(String.format("当前时间：%d点，加班，疲惫之极", work.getHour()));
			} else {
				new StateForeLeave().handle(work);
			}
		}
	}

}
