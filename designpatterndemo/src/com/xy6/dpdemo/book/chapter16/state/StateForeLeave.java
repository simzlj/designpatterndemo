package com.xy6.dpdemo.book.chapter16.state;

/**
 * 强制离开公司状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateForeLeave implements IState {

	@Override
	public void handle(Work work) {
		System.out.println(String.format("当前时间：%d点，公司规定，全体员工立即离开", work.getHour()));
	}

}
