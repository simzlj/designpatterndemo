package com.xy6.dpdemo.book.chapter16.state;

/**
 * 瞌睡工作状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateSleepWork implements IState {

	@Override
	public void handle(Work work) {
		System.out.println(String.format("当前时间：%d点，不行了，睡着了", work.getHour()));
	}

}
