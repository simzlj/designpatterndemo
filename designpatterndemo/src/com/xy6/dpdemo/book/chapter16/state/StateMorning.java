package com.xy6.dpdemo.book.chapter16.state;

/**
 * 上午工作状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateMorning implements IState {
	
	@Override
	public void handle(Work work) {
		if(work.getHour() < 12){
			System.out.println(String.format("当前时间：%d点，上午工作精神百倍", work.getHour()));
		} else {
			new StateNoon().handle(work);
		}
	}
}
