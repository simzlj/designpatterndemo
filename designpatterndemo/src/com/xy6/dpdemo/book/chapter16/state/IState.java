package com.xy6.dpdemo.book.chapter16.state;

public interface IState {

	public void handle(Work work);
	
}
