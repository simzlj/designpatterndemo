package com.xy6.dpdemo.book.chapter16.state;

/**
 * 下午工作状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateAfternoon implements IState {
	
	@Override
	public void handle(Work work) {
		if(work.getHour() < 17){
			System.out.println(String.format("当前时间：%d点，下午状态良好，继续努力", work.getHour()));
		} else {
			new StateDusk().handle(work);
		}
	}
}
