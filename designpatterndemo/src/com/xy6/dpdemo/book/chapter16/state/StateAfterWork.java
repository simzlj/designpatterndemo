package com.xy6.dpdemo.book.chapter16.state;

/**
 * 下班状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateAfterWork implements IState {

	@Override
	public void handle(Work work) {
		System.out.println(String.format("当前时间：%d点，完成工作，按时回家", work.getHour()));
	}
	
}
