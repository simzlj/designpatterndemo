package com.xy6.dpdemo.book.chapter16.state;

/**
 * 中午工作状态
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class StateNoon implements IState {
	
	@Override
	public void handle(Work work) {
		if(work.getHour() < 13){
			System.out.println(String.format("当前时间：%d点，饿了，午饭，犯困，午休", work.getHour()));
		} else {
			new StateAfternoon().handle(work);
		}
	}
}
