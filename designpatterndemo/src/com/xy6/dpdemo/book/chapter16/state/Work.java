package com.xy6.dpdemo.book.chapter16.state;

/**
 * 工作类
 * <p>包含时间点、工作是否完成等熟悉
 * 
 * @author zhang
 *
 */
public class Work {

	/**
	 * 当前时间，几点
	 */
	private int hour;
	
	/**
	 * 工作是否完成
	 */
	private boolean finished;
	
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	/**
	 * 写代码
	 * <p>根据不同时间点，输出工作状态。状态模式。
	 */
	public void program(){
		new StateMorning().handle(this);
	}
	
}
