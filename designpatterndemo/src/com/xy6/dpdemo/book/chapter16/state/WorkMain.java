package com.xy6.dpdemo.book.chapter16.state;

/**
 * 设计一个获取工作状态的程序
 * <p>设置时间点，输出当前工作状态。可以设置为加班、不加班
 * 
 * <pre>
 * 创建一个类，添加时间点属性，添加获取工作状态的方法。在方法中判断时间点，if-else，输出不同时间点的工作状态。
 * 面向过程的设计。如果上下班时间调整，方法需要修改，容易出错。
 * 改：将不同工作状态封装为状态类，实现状态抽象接口。在每个工作状态类中，判断是否为当前状态对应的时间点，是，输出状态，
 * 不是，流向下一个工作状态类。状态模式。
 * 模式本质：为了消除庞大的条件分支语句，状态模式把各种状态转移逻辑分布到State的子类之间。
 * 场景：当一个对象的行为取决于它的状态，并且必须在运行时刻根据状态改变行为时，就可以考虑使用状态模式。
 * 
 * 状态模式：当一个对象的内在状态改变时，允许改变其行为，看起来像是改变了这个对象的类。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-23
 */
public class WorkMain {

	public static void main(String[] args) {
		Work work = new Work();
		work.setHour(8);
		work.program();
		work.setHour(11);
		work.program();
		work.setHour(13);
		work.program();
		work.setHour(17);
		work.program();
		work.setHour(23);
		work.program();
		
		// 工作完成
		work.setFinished(true);
		work.setHour(17);
		work.program();
		work.setHour(18);
		work.program();
	}

}
