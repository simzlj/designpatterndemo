package com.xy6.dpdemo.book.chapter27.interpreter.music;

/**
 * 工厂类
 * <p>生成表达式类
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class ExpressionFactory {

	public static Expression get(String str){
		Expression exp = null;
		// 当首字符是O时，表达式实例化为音节
		// 当首字符是CDEFGAB时，表达式实例化为音符
		switch (str) {
			case "O":
				exp = new ExpressionScale();
				break;
			case "T":
				exp = new ExpressionSpeed();
				break;
			case "C":
			case "D":
			case "E":
			case "F":
			case "G":
			case "A":
			case "B":
			case "P":
				exp = new ExpressionNote();
				break;
		}
		return exp;
	}
	
}
