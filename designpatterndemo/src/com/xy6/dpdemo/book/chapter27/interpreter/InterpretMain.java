package com.xy6.dpdemo.book.chapter27.interpreter;

import java.util.ArrayList;
import java.util.List;

import com.xy6.dpdemo.book.chapter27.interpreter.music.ExpressionFactory;
import com.xy6.dpdemo.book.chapter27.interpreter.music.PlayContext;
import com.xy6.dpdemo.book.chapter27.interpreter.sample.AbstractExpression;
import com.xy6.dpdemo.book.chapter27.interpreter.sample.Context;
import com.xy6.dpdemo.book.chapter27.interpreter.sample.NonTerminalExpression;
import com.xy6.dpdemo.book.chapter27.interpreter.sample.TerminalExpression;

/**
 * 设计一个音乐解释器，解释一段文字，有音节、音符、音速
 * 
 * <pre>
 * 乐谱中元素种类是固定的，规则是固定的：
 * 音阶有低音、中音、高音；P表示休止符，C D E F G A B表示Do Re Mi Fa So La Ti；
 * 音符长度1表示一拍，2表示二拍，0.5表示半拍，0.25表示四分之一拍
 * 
 * 解释器模式，给定一个语言，定义它的文法的一种表示，并定义一个解释器，这个解释器使用该
 * 表示来解释语言中的句子。
 * 当一个语言需要解释执行，并且你可将语言中的句子表示为一个抽象语法树时，可使用解释器模式。
 * 应用：正则表达式，浏览器。用于SQL解析，符号处理引擎
 * 一段文法让程序翻译为具体的指令来执行。
 * 文法：编程语言(java)，程序：编译程序(java编译器)，执行：执行程序(jvm)
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class InterpretMain {

	public static void main(String[] args) {
		test2();
	}

	public static void test1() {
		Context context = new Context();
		List<AbstractExpression> list = new ArrayList<>();
		list.add(new TerminalExpression());
		list.add(new NonTerminalExpression());
		list.add(new TerminalExpression());
		list.add(new TerminalExpression());

		for (AbstractExpression elem : list) {
			elem.interpret(context);
		}
	}

	public static void test2() {
		PlayContext context = new PlayContext();
		context.setText("T 500 O 2 E 0.5 G 0.5");
		while (context.getText().length() > 0) {
			String str = context.getText().substring(0, 1);
			ExpressionFactory.get(str).interpret(context);
		}
	}

}
