package com.xy6.dpdemo.book.chapter27.interpreter.music;

/**
 * 音速
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class ExpressionSpeed extends Expression {

	@Override
	public void execute(String key, Double value) {
		String speed = "";
		if(value < 500){
			speed = "快速";
		} else if(value >= 1000){
			speed = "慢速";
		} else {
			speed = "中速";
		}
		System.out.print(String.format("%s ", speed));
	}

}
