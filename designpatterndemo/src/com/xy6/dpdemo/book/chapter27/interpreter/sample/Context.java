package com.xy6.dpdemo.book.chapter27.interpreter.sample;

/**
 * 全局类
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class Context {

	private String input;
	
	private String output;

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	@Override
	public String toString() {
		return "Context [input=" + input + ", output=" + output + "]";
	}
	
}
