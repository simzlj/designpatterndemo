package com.xy6.dpdemo.book.chapter27.interpreter.sample;

/**
 * 终结符表达式
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class TerminalExpression extends AbstractExpression {

	@Override
	public void interpret(Context context) {
		System.out.println("终结符解释器");
	}

}
