package com.xy6.dpdemo.book.chapter27.interpreter.music;

/**
 * 表达式
 * 
 * @author zhang
 * @since 2017-07-18
 */
public abstract class Expression {

	public void interpret(PlayContext context){
		if(context.getText().length() == 0){
			return;
		} else {
			// 解析第1、2个字符。O 1 C 0.5 D 2
			String text = context.getText();
			String sep = " ";
			// 获取O
			int index1 = text.indexOf(sep);
			String key = text.substring(0, index1);
			String text2 = text.substring(index1 + 1);
			// 获取1
			int index2 = text2.indexOf(sep);
			String value = "";
			if(index2 == -1){
				value = text2;
				context.setText("");
			} else {
				value = text2.substring(0, index2);
				// 删除已解析出的字符
				context.setText(text2.substring(index2 + 1));
			}
			
			execute(key, Double.valueOf(value));
		}
	}
	
	public abstract void execute(String key, Double value);
	
}
