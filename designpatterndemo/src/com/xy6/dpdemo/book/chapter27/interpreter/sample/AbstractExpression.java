package com.xy6.dpdemo.book.chapter27.interpreter.sample;

/**
 * 表达式抽象类
 * 
 * @author zhang
 * @since 2017-07-18
 */
public abstract class AbstractExpression {

	public abstract void interpret(Context context);
	
}
