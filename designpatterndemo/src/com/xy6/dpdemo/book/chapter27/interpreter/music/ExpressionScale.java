package com.xy6.dpdemo.book.chapter27.interpreter.music;

/**
 * 音节
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class ExpressionScale extends Expression {

	@Override
	public void execute(String key, Double value) {
		String scale = "";
		switch(value.intValue()){
			case 1:
				scale = "低音";
				break;
			case 2:
				scale = "中音";
				break;
			case 3:
				scale = "高音";
				break;
			default:
				break;
		}
		System.out.print(String.format("%s ", scale));
	}

}
