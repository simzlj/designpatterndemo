package com.xy6.dpdemo.book.chapter27.interpreter.music;

/**
 * 演奏内容
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class PlayContext {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "PlayContext [text=" + text + "]";
	}
	
}
