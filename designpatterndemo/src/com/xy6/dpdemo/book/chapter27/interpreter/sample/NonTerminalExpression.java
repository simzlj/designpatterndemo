package com.xy6.dpdemo.book.chapter27.interpreter.sample;

/**
 * 非终结符表达式
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class NonTerminalExpression extends AbstractExpression {

	@Override
	public void interpret(Context context) {
		System.out.println("非终结符解释器");
	}

}
