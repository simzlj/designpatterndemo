package com.xy6.dpdemo.book.chapter27.interpreter.music;

/**
 * 音符
 * 
 * @author zhang
 * @since 2017-07-18
 */
public class ExpressionNote extends Expression {

	@Override
	public void execute(String key, Double value) {
		String note = "";
		switch(key){
			case "C":
				note = "1";
				break;
			case "D":
				note = "2";
				break;
			case "E":
				note = "3";
				break;
			case "F":
				note = "4";
				break;
			case "G":
				note = "5";
				break;
			case "A":
				note = "6";
				break;
			case "B":
				note = "7";
				break;
			default:
				break;
		}
		System.out.print(String.format("%s ", note));
	}
	
}
