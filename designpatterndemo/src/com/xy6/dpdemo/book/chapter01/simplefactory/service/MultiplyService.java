package com.xy6.dpdemo.book.chapter01.simplefactory.service;

/**
 * 乘运算service
 * 
 * @author zhang
 * @since 2017-06-10
 */
public class MultiplyService extends CalcSuper {

	@Override
	public Double calc(double d1, double d2) {
		return d1 * d2;
	}

}
