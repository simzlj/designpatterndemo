package com.xy6.dpdemo.book.chapter01.simplefactory.service;

/**
 * 除运算service
 * 
 * @author zhang
 * @since 2017-06-10
 */
public class DivideService extends CalcSuper {

	@Override
	public Double calc(double d1, double d2) throws Exception {
		if(d2 == 0){
			throw new Exception("除数不能为0");
		}
		return d1/d2;
	}

}
