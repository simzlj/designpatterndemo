package com.xy6.dpdemo.book.chapter01.simplefactory.service;

/**
 * 平方运算service
 * 
 * @author zhang
 * @since 2017-06-10
 */
public class SquareService extends CalcSuper {
	
	public Double calc(double d1) throws Exception{
		return d1 * d1;
	};

}
