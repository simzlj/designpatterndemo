package com.xy6.dpdemo.book.chapter01.simplefactory.service;

/**
 * 算术运算服务抽象接口
 * <p>定义计算操作，具体加减乘除运算在实现类中进行，达到新增运算类，无需修改已有的类
 * 
 * @author zhang
 * @since 2017-06-10
 */
public abstract class CalcSuper {
	
	/**
	 * 定义一个数运算接口
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public Double calc(double d) throws Exception{
		return null;
	}
	
	/**
	 * 定义两数运算接口
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public Double calc(double d1, double d2) throws Exception{
		return null;
	};

}
