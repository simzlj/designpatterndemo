package com.xy6.dpdemo.book.chapter01.simplefactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.xy6.dpdemo.book.chapter01.simplefactory.service.CalcFactory;
import com.xy6.dpdemo.book.chapter01.simplefactory.service.CalcSuper;


/**
 * 设计一个计算器控制台程序
 * <p>输入两个数、操作符，输出计算结果
 * 
 * <pre>
 * 设计演变过程：
 * 编写一个类，所有实现都在这一个类中。问题：不能复用，运算代码只能在控制台app中使用，不能在其他app中
 * 改：将运算相关操作在另一个类中实现，封装。问题：添加一种运算，影响所有代码
 * 改：将运算抽象为一个接口，具体运算实现逻辑在一个个的实现类中进行，多态。工厂模式
 * 
 * 简单工厂模式：到底要实例化谁，将来会不会增加实例化的对象，比如增加开根运算，这是容易变化的地方，
 * 应该考虑用一个单独的类来做这个创造实例的过程，这就是工厂。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-10
 */
public class CalcMain {
	
	public static void main(String[] args) {
		if(null == args || args.length < 3){
			System.err.println("args size less than 3");
			return;
		}
		// 解析入参
		String strData1 = args[0];
		String strData2 = args[1];
		String strOperate = args[2];
		double d1 = 0d;
		double d2 = 0d;
		
		// 校验入参
		try{
			d1 = Double.valueOf(strData1);
			d2 = Double.valueOf(strData2);
		} catch(Exception ex){
			System.err.println("error input param");
			return;
		}
		List<String> list = new ArrayList<>(4);
		String[] arr = new String[]{"+", "-", "*", "/"};
		list = Arrays.asList(arr);
		if(!list.contains(strOperate)){
			System.err.println("invalid input operate");
			return;
		}
		
		// 计算结果
		try {
			CalcSuper calcService = CalcFactory.getService(strOperate);
			double result = calcService.calc(d1, d2);
			System.out.println("calc result:" + result);
		} catch (Exception e) {
			System.err.println("occur exception:" + e.getMessage());
			return;
		}
	}

}
