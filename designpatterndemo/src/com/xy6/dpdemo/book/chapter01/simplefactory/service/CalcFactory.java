package com.xy6.dpdemo.book.chapter01.simplefactory.service;


/**
 * 运算服务工厂类
 * <p>根据操作符获取各运算接口
 * 
 * @author zhang
 * @since 2017-06-10
 */
public class CalcFactory {
	
	private CalcFactory(){
	}

	/**
	 * 获取具体运算实现接口
	 * 
	 * @param operate
	 * @return
	 */
	public static CalcSuper getService(String operate) {
		CalcSuper calcService = null;
		switch (operate) {
		case "+":
			calcService = new AddService();
			break;
		case "-":
			calcService = new SubscaleService();
			break;
		case "*":
			calcService = new MultiplyService();
			break;
		case "/":
			calcService = new DivideService();
			break;
		case "^":
			calcService = new SquareService();
			break;
		default:
			break;
		}

		return calcService;
	}

}
