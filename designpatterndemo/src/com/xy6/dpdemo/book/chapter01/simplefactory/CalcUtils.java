package com.xy6.dpdemo.book.chapter01.simplefactory;

/**
 * 算术运算工具类
 * <p>加减乘除操作
 * 
 * @author zhang
 * @since 2017-06-10
 */
public class CalcUtils {

	private CalcUtils(){
	}
	
	public static Double calc(double d1, double d2, String operate) throws Exception{
		Double result = null;
		switch(operate){
			case "+":
				result = CalcUtils.add(d1, d2);
				break;
			case "-":
				result = CalcUtils.subscale(d1, d2);
				break;
			case "*":
				result = CalcUtils.multiply(d1, d2);
				break;
			case "/":
				result = CalcUtils.divide(d1, d2);
				break;
			default: 
				break;
		}
		return result;
	}
	
	public static double add(double d1, double d2) {
		return d1 + d2;
	}
	
	public static double subscale(double d1, double d2) {
		return d1 - d2;
	}
	
	public static double multiply(double d1, double d2) {
		return d1 + d2;
	}
	
	public static double divide(double d1, double d2) throws Exception {
		if(d2 == 0){
			throw new Exception("除0异常");
		}
		return d1 / d2;
	}

}
