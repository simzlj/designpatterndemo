package com.xy6.dpdemo.book.chapter18.memento;

/**
 * 角色状态
 * <p>保存某一时刻角色的生命力等属性
 * 
 * @author zhang
 * @since 2017-06-25
 */
public class RolerState {

	/** 生命力 */
	private Integer vitality;
	
	/** 攻击力 */
	private Integer attack;
	
	/** 防御力 */
	private Integer defence;

	public Integer getVitality() {
		return vitality;
	}

	public void setVitality(Integer vitality) {
		this.vitality = vitality;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getDefence() {
		return defence;
	}

	public void setDefence(Integer defence) {
		this.defence = defence;
	}

	@Override
	public String toString() {
		return "RolerState [vitality=" + vitality + ", attack=" + attack + ", defence=" + defence + "]";
	}

}
