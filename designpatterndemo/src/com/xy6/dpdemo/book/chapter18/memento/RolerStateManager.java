package com.xy6.dpdemo.book.chapter18.memento;

/**
 * 角色状态管理器
 * 
 * @author zhang
 * @since 2017-06-25
 */
public class RolerStateManager {

	private RolerState state;

	public RolerState getState() {
		return state;
	}

	public void setState(RolerState state) {
		this.state = state;
	}
	
}
