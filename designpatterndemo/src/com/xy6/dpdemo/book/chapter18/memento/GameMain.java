package com.xy6.dpdemo.book.chapter18.memento;

/**
 * 设计一个游戏程序
 * <p>游戏角色有生命力、攻击力、防御力等属性，可以保存当前状态，
 * <p>打完boss后，各属性值归0，可以恢复到上次保存的状态，重新打boss。
 * 
 * <pre>
 * 创建一个角色类，有生命力、攻击力等属性，添加一个角色类型的变量，用于保存当前对象。
 * 角色类实现clone接口，创建备份时，clone一个对象赋值给角色变量。恢复备份时，将角色变量赋值给当前对象。
 * 缺点：clone时保存了对象所有属性，有些场景下只保存对象的部分属性；如果备份时保存所有实现，恢复时，恢复指定属性，
 * 浪费了存储空间，且存在数据不安全；
 * 改：创建一个角色状态类，与角色类类似，但是只有需要保存的属性，而非角色所有的属性。角色类中添加创建备份、从备份恢复
 * 的方法，这是角色的两个行为。创建一个角色管理类，用于保存角色状态。备份角色时，将数据保存到管理类中，恢复时，从管理类获取
 * 备份的数据。
 * 
 * 备忘录模式，在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态。这样以后就可以将该对象恢复
 * 到原先保存的状态。
 * 最大作用：当角色的状态改变时，有可能改变后状态无效了，这时候可以使用存储起来的备忘录对状态进行恢复。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-25
 */
public class GameMain {

	public static void main(String[] args) {
		// 打boss前
		Roler roler = new Roler();
		roler.initState();
		System.out.println(roler);
		
		// 保存进度
		RolerStateManager sm = new RolerStateManager();
		sm.setState(roler.saveState());
		System.out.println(roler);
		
		// 打boss，损耗严重
		roler.fightBoss();
		System.out.println(roler);
		
		// 恢复进度
		roler.recoverState(sm.getState());
		System.out.println(roler);
	}

}
