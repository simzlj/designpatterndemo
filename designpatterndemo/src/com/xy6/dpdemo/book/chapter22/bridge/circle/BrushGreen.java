package com.xy6.dpdemo.book.chapter22.bridge.circle;

/**
 * 绿色画笔
 * 
 * @author zhang
 * @since 2017-07-06
 */
public class BrushGreen extends Brush {

	@Override
	public void draw() {
		System.out.println("draw with a green brush");
	}

}
