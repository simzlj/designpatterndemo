package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * 游戏类
 * 
 * @author zhang
 * @since 2017-07-05
 */
public class SoftGame extends Software {

	@Override
	public void run() {
		System.out.println("运行手机游戏");
	}

}
