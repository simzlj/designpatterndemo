package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * 手机品牌抽象接口
 * 
 * @author zhang
 * @since 2017-07-05
 */
public abstract class Brand {

	protected Software soft;
	
	public Brand(Software soft){
		this.soft = soft;
	}
	
	public abstract void show();
	
}
