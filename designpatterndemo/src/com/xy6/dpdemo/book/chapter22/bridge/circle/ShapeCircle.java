package com.xy6.dpdemo.book.chapter22.bridge.circle;

/**
 * 圆形
 * 
 * @author zhang
 * @since 2017-07-06
 */
public class ShapeCircle extends Shape {

	private int x;
	private int y;
	private int r;
	
	public ShapeCircle(Brush brush) {
		super(brush);
	}
	
	public void set(int x, int y, int r){
		this.x = x;
		this.y = y;
		this.r = r;
	}

	@Override
	public void draw() {
		System.out.println("画圆");
		brush.draw();
		System.out.println(String.format("point is (%d,%d), radius is %d", this.x, this.y, this.r));
	}

}
