package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * 软件抽象接口
 * 
 * @author zhang
 * @since 2017-07-05
 */
public abstract class Software {

	public abstract void run();
	
}
