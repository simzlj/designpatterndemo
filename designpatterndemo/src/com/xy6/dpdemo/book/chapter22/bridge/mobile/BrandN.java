package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * N品牌手机
 * 
 * @author zhang
 * @since 2017-07-05
 */
public class BrandN extends Brand {

	public BrandN(Software soft) {
		super(soft);
	}

	@Override
	public void show() {
		System.out.println("N品牌");
		super.soft.run();
	}
	
}
