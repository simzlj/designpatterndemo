package com.xy6.dpdemo.book.chapter22.bridge.circle;

/**
 * 红色画笔
 * 
 * @author zhang
 * @since 2017-07-06
 */
public class BrushRed extends Brush {

	@Override
	public void draw() {
		System.out.println("draw with a red brush");
	}

}
