package com.xy6.dpdemo.book.chapter22.bridge.circle;


/**
 * 设计一个程序，画出不同颜色的圆
 * <p>不同颜色的笔画不同颜色的圆
 * 
 * <pre>
 * 抽象画笔接口，定义各种颜色的画笔。抽象图形接口，定义各种类型的图形。
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-06
 */
public class CircleMain {

	public static void main(String[] args) {
		// 画一个红色的圆
		Brush redBrush = new BrushRed();
		ShapeCircle circle = new ShapeCircle(redBrush);
		circle.set(0, 0, 10);
		circle.draw();
		
		// 画一个绿色的矩形
		Brush greenBrush = new BrushGreen();
		ShapeRectangle rec = new ShapeRectangle(greenBrush);
		rec.set(10, 20);
		rec.draw();
	}

}
