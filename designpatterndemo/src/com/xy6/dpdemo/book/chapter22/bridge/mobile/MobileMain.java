package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * 设计一个程序，模拟手机品牌与手机软件的关系
 * <p>有若干手机品牌，每个品牌的手机都有手机软件，如通讯录、游戏
 * 
 * <pre>
 * 定义手机品牌接口，各品牌实现该接口，每个品牌下有多个子类：通讯录、游戏。缺点：类过多，排列组合，m*n。
 * 违反开放-关闭原则，增加一种软件，需要在m个品牌下各增加一个类。
 * 改：手机与软件分离，定义手机接口，各品牌手机实现该接口。定义软件接口，各软件实现该接口。软件是手机的一部分，
 * 在手机接口中增加一个软件类型的属性，各品牌手机类中也会有一个软件类型的变量。某品牌手机的某个软件，
 * 通过手机接口+软件接口动态组装出来，而非通过继承手机接口、软件接口预先编译好。减少了类的个数，使得手机、软件
 * 可以各自变化。
 * 组合模式+桥接模式。尽量使用组合/聚合，而非继承。
 * 
 * 桥接模式：将抽象部分与它的实现部分分离，使它们可以独立地变化。
 * 该类型的设计模式属于结构型模式，通过提供抽象化和实现化之间的桥接结构，来实现二者的解耦。
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-05
 */
public class MobileMain {
	
	public static void main(String[] args){
		Software game = new SoftGame();
		
		Brand brandM = new BrandM(game);
		brandM.show();
		
		Brand brandN = new BrandN(game);
		brandN.show();
	}
	
}
