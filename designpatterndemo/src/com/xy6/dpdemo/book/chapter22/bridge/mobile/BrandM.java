package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * M品牌手机
 * 
 * @author zhang
 * @since 2017-07-05
 */
public class BrandM extends Brand {

	public BrandM(Software soft) {
		super(soft);
	}

	@Override
	public void show() {
		System.out.println("M品牌");
		super.soft.run();
	}
	
}
