package com.xy6.dpdemo.book.chapter22.bridge.circle;

/**
 * 矩形
 * 
 * @author zhang
 * @since 2017-07-06
 */
public class ShapeRectangle extends Shape {

	private int x;
	private int y;
	
	public ShapeRectangle(Brush brush) {
		super(brush);
	}

	public void set(int x, int y){
		this.x = x;
		this.y = y;
	}

	@Override
	public void draw() {
		System.out.println("画矩形");
		brush.draw();
		System.out.println(String.format("width is %d, length is %d", this.x, this.y));
	}

}
