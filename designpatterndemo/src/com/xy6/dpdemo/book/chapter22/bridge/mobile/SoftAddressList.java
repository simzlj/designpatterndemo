package com.xy6.dpdemo.book.chapter22.bridge.mobile;

/**
 * 通讯录类
 * 
 * @author zhang
 * @since 2017-07-05
 */
public class SoftAddressList extends Software {

	@Override
	public void run() {
		System.out.println("运行手机通讯录");
	}

}
