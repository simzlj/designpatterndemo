package com.xy6.dpdemo.book.chapter22.bridge.circle;

/**
 * 画笔接口
 * <p>定义各颜色画笔共有的行为
 * 
 * @author zhang
 * @since 2017-07-06
 */
public abstract class Brush {

	public abstract void draw();
	
}
