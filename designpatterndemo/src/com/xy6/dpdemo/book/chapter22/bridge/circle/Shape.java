package com.xy6.dpdemo.book.chapter22.bridge.circle;

/**
 * 图形接口
 * <p>定义各图形共有的行为
 * 
 * @author zhang
 * @since 2017-07-06
 */
public abstract class Shape {

	protected Brush brush;
	
	public Shape(Brush brush){
		this.brush = brush;
	}
	
	public abstract void draw();
	
}
