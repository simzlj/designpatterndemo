package com.xy6.dpdemo.book.chapter13.builder;

/**
 * 设计一个创建动画人物的程序
 * 人物由头、身体、胳膊、腿组成，可以创建瘦的、高的人物
 * 
 * <pre>
 * 创建人物接口，定义人物部位函数，创建不同类型的具体的人物类，实现人物接口，画出各部位。
 * 创建一个控制类，定义创建一个人物的流程，头-身体-胳膊-腿。
 * 
 * 建造者模式/生成器模式，将一个复杂对象的构建与它的表示分离，使得同样的建造过程可以创建不同的表示。
 * 建造者模式主要用于创建一些复杂的对象，这些对象内部组件的构建顺序通常是稳定的，但是对象内部的构建
 * 通常面临复杂的变化。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class BuilderMain {

	public static void main(String[] args){
		BuilderDirector director = new BuilderDirector(new PersonThinBuilder());
		director.createPerson();
		
		System.out.println();
		
		BuilderDirector director2 = new BuilderDirector(new PersonTallBuilder());
		director2.createPerson();
	}
	
}
