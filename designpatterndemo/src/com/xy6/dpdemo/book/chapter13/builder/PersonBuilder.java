package com.xy6.dpdemo.book.chapter13.builder;

/**
 * 建造者接口
 * <p>定义产品的各种行为
 * 
 * @author zhang
 * @since 2017-06-20
 */
public abstract class PersonBuilder {

	public abstract void buildHead();
	
	public abstract void buildBody();
	
	public abstract void buildLeftArm();
	
	public abstract void buildRightArm();
	
	public abstract void buildLeftLeg();
	
	public abstract void buildRightLeg();
	
}
