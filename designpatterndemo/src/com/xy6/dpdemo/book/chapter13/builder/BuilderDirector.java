package com.xy6.dpdemo.book.chapter13.builder;

/**
 * 建造控制类
 * <p>控制产品的建造过程，不负责产品内部组件的创建
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class BuilderDirector {

	PersonBuilder builder;
	
	public BuilderDirector(PersonBuilder builder){
		this.builder = builder;
	}
	
	public void createPerson(){
		builder.buildHead();
		builder.buildBody();
		builder.buildLeftArm();
		builder.buildRightArm();
		builder.buildLeftLeg();
		builder.buildRightLeg();
	}
	
}
