package com.xy6.dpdemo.book.chapter13.builder;

/**
 * 建造瘦的动画人物service
 * <p>继承建造类，实现各部件具体创造过程
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class PersonThinBuilder extends PersonBuilder {

	@Override
	public void buildHead() {
		System.out.println("build head");
	}

	@Override
	public void buildBody() {
		System.out.println("build fat body");
	}

	@Override
	public void buildLeftArm() {
		System.out.println("build left arm");
	}

	@Override
	public void buildRightArm() {
		System.out.println("build right arm");
	}

	@Override
	public void buildLeftLeg() {
		System.out.println("build left leg");
	}

	@Override
	public void buildRightLeg() {
		System.out.println("build right leg");
	}

}
