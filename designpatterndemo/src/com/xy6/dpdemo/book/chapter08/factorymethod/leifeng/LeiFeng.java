package com.xy6.dpdemo.book.chapter08.factorymethod.leifeng;

/**
 * 功能类接口
 * 
 * @author zhang
 * @since 2017-06-16
 */
public abstract class LeiFeng {

	public void sweep(){
		System.out.println("sweep room...");
	}
	
	public void cook(){
		System.out.println("cook dinner...");
	}
	
}
