package com.xy6.dpdemo.book.chapter08.factorymethod.calc;

/**
 * 加法工厂
 * 
 * @author zhang
 * @since 2017-06-16
 */
public class AddFactory implements IFactory {

	@Override
	public Operation CreateOperation() {
		return new OperationAdd();
	}
	
}
