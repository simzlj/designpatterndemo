package com.xy6.dpdemo.book.chapter08.factorymethod.calc;

/**
 * 功能类接口
 * 
 * @author zhang
 * @since 2017-06-16
 */
public abstract class Operation {
	
	public Double calc(double d1, double d2) {
		return null;
	};

}
