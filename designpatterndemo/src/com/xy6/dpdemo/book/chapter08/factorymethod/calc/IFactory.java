package com.xy6.dpdemo.book.chapter08.factorymethod.calc;

/**
 * 工厂接口
 * 
 * @author zhang
 * @since 2017-06-16
 */
public interface IFactory {

	public Operation CreateOperation();
	
}
