package com.xy6.dpdemo.book.chapter08.factorymethod.calc;

/**
 * 加法类
 * 
 * @author zhang
 * @since 2017-06-16
 */
public class OperationAdd extends Operation {

	public Double calc(double d1, double d2) {
		return d1 + d2;
	};
	
}
