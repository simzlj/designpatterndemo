package com.xy6.dpdemo.book.chapter08.factorymethod.leifeng;

/**
 * 志愿者工厂类
 * 
 * @author zhang
 * @since 2017-06-16
 */
public class VolunteerFactory implements IFactory {

	@Override
	public LeiFeng CreateOperation() {
		return new Volunteer();
	}

}
