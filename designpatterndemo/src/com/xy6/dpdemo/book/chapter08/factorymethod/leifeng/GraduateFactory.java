package com.xy6.dpdemo.book.chapter08.factorymethod.leifeng;

/**
 * 毕业生工厂类
 * 
 * @author zhang
 * @since 2017-06-16
 */
public class GraduateFactory implements IFactory {

	@Override
	public LeiFeng CreateOperation() {
		return new Graduate();
	}
	
}
