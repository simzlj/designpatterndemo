package com.xy6.dpdemo.book.chapter08.factorymethod.leifeng;

/**
 * 工厂类接口
 * 
 * @author zhang
 * @since 2017-06-16
 */
public interface IFactory {

	public LeiFeng CreateOperation();
	
}
