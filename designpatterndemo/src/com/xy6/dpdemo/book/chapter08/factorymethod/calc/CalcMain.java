package com.xy6.dpdemo.book.chapter08.factorymethod.calc;

/**
 * 设计一个计算器程序
 * <p>使用工厂方法模式实现，而非简单工厂模式
 * 
 * <pre>
 * 将判断移到客户端进行，由客户端决定使用那种类型的对象。
 * 当增加一种运算时，对于简单工厂模式，需要修改工厂类中的分支，违背了开放-封闭原则，
 * 对于工厂方法模式，不需要修改现有工厂代码，在客户端直接使用。
 * 缺点：每增加一个产品，就要增加一个产品工厂类，增加了额外工作量。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-16
 */
public class CalcMain {

	public static void main(String[] args) {
		IFactory factory = new AddFactory();
		Operation add = factory.CreateOperation();
		Double d = add.calc(1d, 2d);
		System.out.println(d);
		
		IFactory factorySub = new SubFactory();
		Operation sub = factorySub.CreateOperation();
		Double d2 = sub.calc(1d, 2d);
		System.out.println(d2);
	}

}
