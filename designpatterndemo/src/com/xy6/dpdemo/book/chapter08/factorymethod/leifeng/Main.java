package com.xy6.dpdemo.book.chapter08.factorymethod.leifeng;

/**
 * 设计一个学雷锋做好事不留名程序
 * 
 * <pre>
 * 做好事有打扫卫生、做饭...。有两类人做好事，大学生，志愿者。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-16
 */
public class Main {

	public static void main(String[] args) {
		IFactory factory1 = new GraduateFactory();
		LeiFeng leifeng1 = factory1.CreateOperation();
		leifeng1.sweep();
		leifeng1.cook();
		
		IFactory factory2 = new GraduateFactory();
		LeiFeng leifeng2 = factory2.CreateOperation();
		leifeng2.sweep();
		leifeng2.cook();
	}

}
