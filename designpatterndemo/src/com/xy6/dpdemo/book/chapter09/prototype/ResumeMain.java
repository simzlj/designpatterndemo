package com.xy6.dpdemo.book.chapter09.prototype;

import java.math.BigDecimal;


/**
 * 设计一个复制简历的程序
 * 
 * <pre>
 * 从一个对象创建另外一个可定制的对象，而无需了解创建的细节。原型模式
 * 通过实现Cloneable接口达到效果，分为浅复制、深复制。
 * clone：不用重新初始化对象，动态地获取对象的状态
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class ResumeMain {

	public static void main(String[] args) {
		Resume resume = new Resume("zhangsan", "male");
		resume.setSalary(BigDecimal.ONE);
		resume.setWorkExp("2013-2015", "comtop");
		
		Resume resume2 = resume.deepClone2();
		resume.setSalary(BigDecimal.TEN);
		resume2.setWorkExp("2015-2017", "dataenergy");
		
		resume.display();
		resume2.display();
	}

}
