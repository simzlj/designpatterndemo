package com.xy6.dpdemo.book.chapter09.prototype;

import java.io.Serializable;

/**
 * 工作经历bean
 * <p>实现Serializable接口，以通过序列化技术实现深度复制
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class WorkExp implements Serializable {
	
	private static final long serialVersionUID = 6396359272844043567L;

	private String timeArea;
	
	private String company;

	public WorkExp(){
	}
	
	public WorkExp(String timeArea, String company){
		this.timeArea = timeArea;
		this.company = company;
	}
	
	public String getTimeArea() {
		return timeArea;
	}

	public void setTimeArea(String timeArea) {
		this.timeArea = timeArea;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "WorkExp [timeArea=" + timeArea + ", company=" + company + "]";
	}
	
}
