package com.xy6.dpdemo.book.chapter06.decorate.shape;

/**
 * 设计一个图形涂色程序，可以为不同图形设置不同颜色
 * 
 * <pre>
 * 意图：动态给一个对象添加一些额外的职责
 * 如何解决：将具体功能职责划分开， 同时继承装饰模式
 * 关键代码：
 * 		1、Component类充当抽象角色，不应该具体实现
 * 		2、修饰类引用和继承Component类，具体扩展类重写父类方法
 * 优点：装饰类和被装饰类可以独立发展，不会相互耦合。
 * 使用场景：1、扩展一个类的功能。2、动态增加功能，动态撤销
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-14
 */
public class ShapeMain {

	public static void main(String[] args) {
		Circle circle = new Circle();
		Shape redCircle = new RedShapeDecorator(new Circle());
		Shape redRectangle = new RedShapeDecorator(new Rectangle());
		
		System.out.println("Circle with normal border:");
		circle.draw();
		
		System.out.println("\nCircle of red border:");
		redCircle.draw();
		
		System.out.println("\nRectangle of red border:");
		redRectangle.draw();
	}

}
