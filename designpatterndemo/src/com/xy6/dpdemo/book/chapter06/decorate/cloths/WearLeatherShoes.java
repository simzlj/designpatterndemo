package com.xy6.dpdemo.book.chapter06.decorate.cloths;

public class WearLeatherShoes extends WearDecorator {

	@Override
	public void show() {
		System.out.print("shoes ");
	}

}
