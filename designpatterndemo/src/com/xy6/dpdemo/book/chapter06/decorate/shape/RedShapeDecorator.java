package com.xy6.dpdemo.book.chapter06.decorate.shape;

public class RedShapeDecorator extends ShapeDecorator {

	public RedShapeDecorator(Shape shape) {
		super(shape);
	}

	public void draw() {
		super.decoratedShape.draw();
		setRedBorder(super.decoratedShape);
	}
	
	private void setRedBorder(Shape decoratedShape){
		System.out.println("Border Color: Red");
	}
	
}
