package com.xy6.dpdemo.book.chapter06.decorate.cloths;

public class WearBigTrouser extends WearDecorator {

	@Override
	public void show() {
		System.out.print("big trouser ");
		super.show();
	}

}
