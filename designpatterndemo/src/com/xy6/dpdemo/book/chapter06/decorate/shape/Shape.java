package com.xy6.dpdemo.book.chapter06.decorate.shape;

public abstract class Shape {

	public abstract void draw();
	
}
