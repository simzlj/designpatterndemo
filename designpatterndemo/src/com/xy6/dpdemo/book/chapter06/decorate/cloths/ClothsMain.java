package com.xy6.dpdemo.book.chapter06.decorate.cloths;

/**
 * 设计一个给人搭配不同服饰的系统
 * 
 * <pre>
 * 设计演变过程：
 * 编写一个装饰抽象类，定义若干具体的衣服类，如T恤、领带、鞋子，扩展抽象类。使用时，new若干具体类，逐个装饰。
 * 缺点：一个一个装饰，而非装饰出完整一套衣服后一起展示，new多少个类，展示多少次，重复。
 * <p>改进：为了避免重复，对具体对象逐个添加衣服，最后一起展示。使用装饰模式。
 * 
 * 装饰模式：动态地给一个对象添加一些额外的职责。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-12
 */
public class ClothsMain {

	public static void main(String[] args) {
		ConcretePerson p1 = new ConcretePerson();
		
		WearDecorator tshirt = new WearTShirt();
		WearDecorator trouser = new WearBigTrouser();
		tshirt.setComponent(p1);
		trouser.setComponent(tshirt);
		trouser.show();
	}

}
