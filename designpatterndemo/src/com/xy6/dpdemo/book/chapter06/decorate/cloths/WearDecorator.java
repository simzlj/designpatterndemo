package com.xy6.dpdemo.book.chapter06.decorate.cloths;

public class WearDecorator extends Person {

	protected Person component;
	
	public void setComponent(Person component){
		this.component = component;
	}
	
	public void show(){
		if(null != component){
			component.show();
		}
	}
	
}
