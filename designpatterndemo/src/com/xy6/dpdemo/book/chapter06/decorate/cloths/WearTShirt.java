package com.xy6.dpdemo.book.chapter06.decorate.cloths;

public class WearTShirt extends WearDecorator {

	@Override
	public void show() {
		System.out.print("t-shirt ");
		super.show();
	}

}
