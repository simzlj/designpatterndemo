package com.xy6.dpdemo.book.chapter06.decorate.shape;

public class ShapeDecorator extends Shape {

	protected Shape decoratedShape;
	
	public ShapeDecorator(Shape shape){
		this.decoratedShape = shape;
	}
	
	@Override
	public void draw() {
		decoratedShape.draw();
	}
	
}
