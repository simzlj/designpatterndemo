package com.xy6.dpdemo.book.chapter14.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 通告人，BOSS
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class NotifierBoss extends Notifier {

	private List<Observer> list = new ArrayList<>(5);
	private String msg;
	
	@Override
	public void attach(Observer obs) {
		list.add(obs);
	}

	@Override
	public void detach(Observer obs) {
		list.remove(obs);
	}

	@Override
	public void notice() {
		for(Observer elem : list){
			elem.change();
		}
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
