package com.xy6.dpdemo.book.chapter14.observer.event;

/**
 * 该类为 门1监听接口的实现，做具体的开门，关门动作
 * 
 * @author zhang
 *
 */
public class DoorListener1 implements IDoorListener {

	@Override
	public void doorEvent(DoorEvent event) {
		if (null != event.getDoorState() && "open".equals(event.getDoorState())) {
			System.out.println("门1打开");
		} else {
			System.out.println("门1关闭");
		}
	}

}
