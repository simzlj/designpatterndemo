package com.xy6.dpdemo.book.chapter14.observer.event;

import java.util.EventObject;

/**
 * 开门、关门事件
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class DoorEvent extends EventObject {

	private static final long serialVersionUID = -8785560373037592804L;
	
	/**
	 * 门的状态，开、关
	 */
	private String doorState = "";

	public DoorEvent(Object source) {
		super(source);
	}

	public DoorEvent(Object source, String doorState) {
		super(source);
		this.doorState = doorState;
	}

	public String getDoorState() {
		return doorState;
	}

	public void setDoorState(String doorState) {
		this.doorState = doorState;
	}

}
