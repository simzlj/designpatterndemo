package com.xy6.dpdemo.book.chapter14.observer;

/**
 * 通告人接口
 * <p>定义添加接收人、删除接收人、通告等行为
 * 
 * @author zhang
 * @since 2017-06-20
 */
public abstract class Notifier {

	/**
	 * 添加接收人
	 * 
	 * @param obs
	 */
	public abstract void attach(Observer obs);
	
	/**
	 * 移除接收人
	 * 
	 * @param obs
	 */
	public abstract void detach(Observer obs);
	
	/**
	 * 通知
	 * 
	 * @param obs
	 */
	public abstract void notice();

	public String getMsg() {
		return null;
	}

	public void setMsg(String msg) {
	}

}
