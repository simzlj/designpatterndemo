package com.xy6.dpdemo.book.chapter14.observer.event2;

import java.util.ArrayList;

/**
 * 秘书通告类
 * <p>秘书通告消息
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class SecretaryNotifier extends NotifierAbstractService {

	public void attach(IBossListener listener){
		if(null == listeners){
			listeners = new ArrayList<>(5);
		}
		listeners.add(listener);
	}
	
	public void detach(IBossListener listener){
		if(null == listeners){
			return;
		}
		listeners.remove(listener);
	}
	
	public void fireBossIsComing(){
		if (listeners == null){
			return;
		}
		BossEvent event = new BossEvent(this, "boss is coming");
		notifyListeners(event);
	}
	
	public void fireStockTalking(){
		if (listeners == null){
			return;
		}
		BossEvent event = new BossEvent(this, "how about your stock ?");
		notifyListeners(event);
	}

}
