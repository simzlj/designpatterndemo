package com.xy6.dpdemo.book.chapter14.observer.event2;

/**
 * 基于事件监听，设计一个观察BOSS是否回来的程序
 * <p>boss回来时，前台秘书通知大家，大家恢复工作
 * 
 * <pre>
 * 创建一个通告人接口，定义通告人具有的行为，创建具体的通告人：boss、秘书。
 * 创建监听器接口，定义监听方法，创建具体的监听人：看股票的人、看NBA的人。
 * 观察者模式。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class BossMain {

	public static void main(String[] args) {
		// boss自己通告
		NotifierAbstractService manager = new BossNotifier();
		manager.attach(new StockListener());
		manager.fireBossIsComing();
		System.out.println();
		
		// 前台秘书通告
		NotifierAbstractService manager2 = new SecretaryNotifier();
		NBAListener nba = new NBAListener();
		manager2.attach(new StockListener());
		manager2.attach(nba);
		manager2.fireBossIsComing();
		System.out.println();
		
		// 发起股票讨论，将看NBA的人从接收人集合中删除
		manager2.detach(nba);
		manager2.fireStockTalking();
	}

}
