package com.xy6.dpdemo.book.chapter14.observer;

/**
 * 正在看股票的观察者
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class ObserverStock extends Observer {

	public ObserverStock(String name, Notifier notify) {
		super(name, notify);
	}

	@Override
	public void change() {
		System.out.println("关闭股票窗口，继续工作。" + super.name + "，" + super.notify.getMsg());
	}

}
