package com.xy6.dpdemo.book.chapter14.observer.event;

/**
 * 该类为 门2监听接口的实现，做具体的开门，关门，以及开灯，关灯动作
 * 
 * @author zhang
 *
 */
public class DoorListener2 implements IDoorListener {

	@Override
	public void doorEvent(DoorEvent event) {
		if (event.getDoorState() != null && event.getDoorState().equals("open")) {
			System.out.println("门2打开，同时打开走廊的灯");
		} else {
			System.out.println("门2关闭，同时关闭走廊的灯");
		}
	}

}
