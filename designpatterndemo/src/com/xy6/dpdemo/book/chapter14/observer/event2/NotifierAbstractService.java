package com.xy6.dpdemo.book.chapter14.observer.event2;

import java.util.List;

/**
 * 通告人抽象类
 * <p>定义通告的各种行为
 * 
 * @author zhang
 * @since 2017-06-21
 */
public abstract class NotifierAbstractService {

	public List<IBossListener> listeners;

	public void attach(IBossListener listener){}
	
	public void detach(IBossListener listener){}
	
	protected void notifyListeners(BossEvent event){
		for(IBossListener elem : listeners){
			elem.listen(event);
		}
	}

	public abstract void fireBossIsComing();
	
	public void fireStockTalking(){}
	
}
