package com.xy6.dpdemo.book.chapter14.observer.event;

/**
 * 设计一个开门、关门的程序
 * <p>开门时触发事件，关门时触发事件
 * 
 * <pre>
 * 基于事件实现观察者模式。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class DoorMain {

	public static void main(String[] args) {
		DoorManager manager = new DoorManager();
		// 给门1增加监听器
		manager.addDoorListener(new DoorListener1());
		// 给门2增加监听器
		manager.addDoorListener(new DoorListener2());
		
		manager.fireWorkspaceOpened();
		System.out.println("我已经进来了");
		manager.fireWorkspaceClosed();
	}

}
