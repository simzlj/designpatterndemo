package com.xy6.dpdemo.book.chapter14.observer.event;

import java.util.EventListener;

/**
 * 定义监听接口，负责监听DoorEvent事件
 * 
 * @author zhang
 *
 */
public interface IDoorListener extends EventListener {

	public void doorEvent(DoorEvent event);
	
}
