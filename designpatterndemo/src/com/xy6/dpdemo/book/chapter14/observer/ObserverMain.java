package com.xy6.dpdemo.book.chapter14.observer;

/**
 * 设计一个观察BOSS是否回来的程序
 * <p>boss回来时，前台秘书通知大家，大家恢复工作
 * 
 * <pre>
 * 创建一个接口，定义通知人的行为。创建另外一个接口，定义接收人的行为。
 * boss、秘书作为具体通知人，实现通知人接口。看股票的同事、看NBA的同事实现接收人接口。
 * 
 * 观察者模式/发布-订阅模式，定义一种一对多的关系，让多个观察者对象同时监听某一主题对象。这个主题对象在发生变化时，
 * 会通知所有观察者，使它们能够自动更新自己。
 * 场景：当一个对象改变，需要同时改变其他对象的时候
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class ObserverMain {

	public static void main(String[] args) {
		Notifier sec = new NotifierSecretary();
		Observer mate1 = new ObserverStock("zhang", sec);
		Observer mate2 = new ObserverStock("wang", sec);
		Observer mate3 = new ObserverNBA("li", sec);
		
		sec.attach(mate1);
		sec.attach(mate2);
		sec.attach(mate3);
		sec.detach(mate2);
		
		sec.setMsg("boss is coming");
		sec.notice();
	}

}
