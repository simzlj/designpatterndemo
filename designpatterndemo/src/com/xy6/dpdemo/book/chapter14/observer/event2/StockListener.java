package com.xy6.dpdemo.book.chapter14.observer.event2;

/**
 * 看股票的人监听类
 * <p>一边监听boss归来，一边看股票的人
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class StockListener implements IBossListener {

	@Override
	public void listen(BossEvent event) {
		System.out.println(event.getMsg());
		if(event.getMsg().indexOf("boss") >= 0){
			System.out.println("关闭股票窗口，继续工作");
		} else {
			System.out.println("我的股票涨得好慢");
		}
	}

}
