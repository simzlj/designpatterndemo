package com.xy6.dpdemo.book.chapter14.observer.event2;

import java.util.EventListener;

/**
 * 监听boss归来的监听器
 * <p>定义监听的行为
 * 
 * @author zhang
 * @since 2017-06-21
 */
public interface IBossListener extends EventListener {

	public void listen(BossEvent event);
	
}
