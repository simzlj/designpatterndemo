package com.xy6.dpdemo.book.chapter14.observer.event2;

import java.util.ArrayList;

/**
 * boss通告类
 * <p>boss自己通告消息
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class BossNotifier extends NotifierAbstractService {

	public void attach(IBossListener listener) {
		if (null == listeners) {
			listeners = new ArrayList<>(5);
		}
		listeners.add(listener);
	}

	public void fireBossIsComing() {
		if (listeners == null) {
			return;
		}
		BossEvent event = new BossEvent(this, "boss is coming");
		notifyListeners(event);
	}

}
