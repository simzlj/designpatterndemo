package com.xy6.dpdemo.book.chapter14.observer;

/**
 * 观察者接口
 * <p>定义接收到通告后，发生的变化行为
 * 
 * @author zhang
 * @since 2017-06-20
 */
public abstract class Observer {

	protected String name;
	
	protected Notifier notify;
	
	public Observer(String name, Notifier notify){
		this.name = name;
		this.notify = notify;
	}
	
	public abstract void change();
	
}
