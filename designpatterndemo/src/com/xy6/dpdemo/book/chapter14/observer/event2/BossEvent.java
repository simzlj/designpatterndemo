package com.xy6.dpdemo.book.chapter14.observer.event2;

import java.util.EventObject;

/**
 * boss归来事件类
 * <p>定义消息变量
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class BossEvent extends EventObject {

	private static final long serialVersionUID = 3221097010018507097L;

	private String msg;
	
	public BossEvent(Object source) {
		super(source);
	}
	
	public BossEvent(Object source, String msg) {
		super(source);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
