package com.xy6.dpdemo.book.chapter14.observer;

/**
 * 正在看NBA的观察者
 * 
 * @author zhang
 * @since 2017-06-20
 */
public class ObserverNBA extends Observer {

	public ObserverNBA(String name, Notifier sub) {
		super(name, sub);
	}

	@Override
	public void change() {
		System.out.println("关闭nba窗口，继续工作。" + super.name + "，" + super.notify.getMsg());
	}

}
