package com.xy6.dpdemo.book.chapter14.observer.event2;

/**
 * 看NBA的人监听类
 * <p>一边监听boss归来，一边看NBA的人
 * 
 * @author zhang
 * @since 2017-06-21
 */
public class NBAListener implements IBossListener {

	@Override
	public void listen(BossEvent event) {
		System.out.println(event.getMsg());
		if(event.getMsg().indexOf("boss") >= 0){
			System.out.println("关闭NBA窗口，继续工作");
		} else {
			System.out.println("不关我的事");
		}
	}

}
