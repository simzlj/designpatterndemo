package com.xy6.dpdemo.book.chapter26.flyweight.web;

/**
 * 共享抽象类，网站
 * 
 * @author zhang
 * @since 2017-07-16
 */
public abstract class Website {

	/**
	 * 类型
	 * <p>共享对象的内部状态
	 */
	protected String type;
	
	public Website(String type){
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public abstract void use(WebsiteUser user);
	
}
