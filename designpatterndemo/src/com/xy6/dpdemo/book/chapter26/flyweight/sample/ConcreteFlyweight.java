package com.xy6.dpdemo.book.chapter26.flyweight.sample;

/**
 * 共享的对象
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class ConcreteFlyweight extends Flyweight {

	@Override
	public void operation(int num) {
		System.out.println("共享的具体flyweight:" + num);
	}

}
