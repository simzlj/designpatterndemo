package com.xy6.dpdemo.book.chapter26.flyweight.sample;

import java.util.Hashtable;

/**
 * 享元工厂类
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class FlyweightFactory {

	private Hashtable<String, Flyweight> flyweights = new Hashtable<>();
	
	public FlyweightFactory(){
		flyweights.put("x", new ConcreteFlyweight());
		flyweights.put("y", new ConcreteFlyweight());
		flyweights.put("z", new ConcreteFlyweight());
	}
	
	public Flyweight getFlyweight(String key){
		return flyweights.get(key);
	}
	
}
