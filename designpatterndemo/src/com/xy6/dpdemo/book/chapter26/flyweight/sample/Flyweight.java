package com.xy6.dpdemo.book.chapter26.flyweight.sample;

/**
 * 抽象接口，定义共享、不共享对象共有的行为
 * 
 * @author zhang
 * @since 2017-07-16
 */
public abstract class Flyweight {

	public abstract void operation(int num);
	
}
