package com.xy6.dpdemo.book.chapter26.flyweight.sample;

/**
 * 不共享的对象
 * <p>解决不需要共享对象的问题
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class UnsharedConcreteFlyweight extends Flyweight {

	@Override
	public void operation(int num) {
		System.out.println("不共享的具体flyweight:" + num);
	}

}
