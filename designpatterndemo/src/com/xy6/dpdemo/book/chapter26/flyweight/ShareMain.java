package com.xy6.dpdemo.book.chapter26.flyweight;

import com.xy6.dpdemo.book.chapter26.flyweight.sample.Flyweight;
import com.xy6.dpdemo.book.chapter26.flyweight.sample.FlyweightFactory;
import com.xy6.dpdemo.book.chapter26.flyweight.sample.UnsharedConcreteFlyweight;
import com.xy6.dpdemo.book.chapter26.flyweight.web.Website;
import com.xy6.dpdemo.book.chapter26.flyweight.web.WebsiteFactory;
import com.xy6.dpdemo.book.chapter26.flyweight.web.WebsiteUser;

/**
 * 设计一个做网站的程序
 * <p>用同一套模板做个人作品网站、企业官方网站、个人博客等，部分功能相同，部分功能不同
 * 
 * <pre>
 * 创建网站类，每次创建网站，new一个新的对象，设置参数。缺点：部分网站相似，只有名称不同，创建多个网站类，
 * 重复存储数据，浪费空间。
 * 改：创建网站抽象类，定义共有的属性、行为，创建具体类，实现抽象接口。创建工厂类，定义一个集合，初始化类时，
 * 将常用类型的网站保存到集合中，网站类型作为key。客户端创建网站时，从工厂类中获取，传入类型。
 * 这样内存中每个类型的网站，只有一个对象。
 * 改：添加网站用户。不同网站用户不同，此为网站的外部状态。在网站抽象方法中添加用户参数，客户端从工厂类获取某个类型的网站使用时，
 * 传入网站的用户。注：在方法中添加用户参数，而非在抽象类中添加用户属性，避免客户端设置用户属性，对共享池中对象造成污染。
 * 
 * 享元模式，运用共享技术有效地支持大量细粒度的对象。
 * 目的：减少创建对象的数量，以减少内存占用，提高性能。
 * 属于结构型模式
 * 应用：字符串string设计，围棋、跳棋游戏，数据库的连接池。
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-15
 */
public class ShareMain {

	public static void main(String[] args) {
		test3();
	}
	
	/**
	 * 面向过程设计
	 */
	public static void test1(){
		MyWebsite s1 = new MyWebsite("个人作品展示");
		s1.use();
		
		MyWebsite s2 = new MyWebsite("官方网站");
		s2.use();
		
		MyWebsite s3 = new MyWebsite("官方网站");
		s3.use();
		
		MyWebsite s4 = new MyWebsite("博客");
		s4.use();
	}
	
	public static void test2(){
		int num = 22;

		FlyweightFactory factory = new FlyweightFactory();
		Flyweight fx = factory.getFlyweight("x");
		fx.operation(--num);
		Flyweight fy = factory.getFlyweight("y");
		fy.operation(--num);
		Flyweight fz = factory.getFlyweight("z");
		fz.operation(--num);
		Flyweight fx2 = factory.getFlyweight("x");
		fx2.operation(--num);

		UnsharedConcreteFlyweight u1 = new UnsharedConcreteFlyweight();
		u1.operation(--num);
	}
	
	public static void test3(){
		Website w1 = WebsiteFactory.get("个人作品展示");
		w1.use(new WebsiteUser("zhangsan"));
		Website w2 = WebsiteFactory.get("官方网站");
		w2.use(new WebsiteUser("lisi"));
		Website w3 = WebsiteFactory.get("官方网站");
		w3.use(new WebsiteUser("wangwu"));
		Website w4 = WebsiteFactory.get("博客");
		w4.use(new WebsiteUser("liuer"));
		
		System.out.println("共享池对象数量:" + WebsiteFactory.count());
	}
	
}
