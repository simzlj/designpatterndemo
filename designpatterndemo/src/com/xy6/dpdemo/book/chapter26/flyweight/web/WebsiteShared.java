package com.xy6.dpdemo.book.chapter26.flyweight.web;

/**
 * 共享的享元类
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class WebsiteShared extends Website {

	public WebsiteShared(String type) {
		super(type);
	}
	
	@Override
	public void use(WebsiteUser user) {
		System.err.println(String.format("网站分类: %s，用户: %s", super.type, user));
	}

}
