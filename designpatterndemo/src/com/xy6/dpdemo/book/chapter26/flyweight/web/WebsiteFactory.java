package com.xy6.dpdemo.book.chapter26.flyweight.web;

import java.util.HashMap;
import java.util.Map;

/**
 * 共享对象工厂类
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class WebsiteFactory {

	/**
	 * 网站模版池
	 */
	private static Map<String, Website> websites = new HashMap<>();
	
	public static Website get(String type){
		if(websites.containsKey(type)){
			System.out.println("从共享池中获取:" + type);
			return websites.get(type);
		} else {
			System.out.println("新建对象，加入共享池:" + type);
			websites.put(type, new WebsiteShared(type));
			return websites.get(type);
		}
	}
	
	public static int count(){
		return websites.size();
	}
	
}
