package com.xy6.dpdemo.book.chapter26.flyweight;

/**
 * 网站对象
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class MyWebsite {

	private String name;
	
	public MyWebsite(){
	}
	
	public MyWebsite(String name){
		this.name = name;
	}
	
	public void use(){
		System.out.println(this.name);
	}
	
}
