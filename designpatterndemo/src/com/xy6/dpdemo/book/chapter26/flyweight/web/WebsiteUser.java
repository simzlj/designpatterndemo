package com.xy6.dpdemo.book.chapter26.flyweight.web;

/**
 * 网站用户
 * <p>共享对象的外部状态，不同对象该值不同
 * 
 * @author zhang
 * @since 2017-07-16
 */
public class WebsiteUser {

	String name;

	public WebsiteUser(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [name=" + name + "]";
	}
	
}
