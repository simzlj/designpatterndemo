package com.xy6.dpdemo.book.chapter19.composite;

import org.apache.commons.lang3.StringUtils;

public class FinanceDepartment extends Company {

	public FinanceDepartment(String name) {
		super(name);
	}

	@Override
	public void add(Company com) {
	}

	@Override
	public void remove(Company com) {
	}

	@Override
	public void display(int depth) {
		System.out.println(StringUtils.repeat("-", depth) + super.name);
	}

	@Override
	public void duty() {
		System.out.println(super.name + "	公司财务收支管理");
	}
	
}
