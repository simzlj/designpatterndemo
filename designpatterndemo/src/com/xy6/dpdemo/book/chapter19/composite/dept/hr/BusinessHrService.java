package com.xy6.dpdemo.book.chapter19.composite.dept.hr;

import com.xy6.dpdemo.book.chapter19.composite.office.Office;

public class BusinessHrService extends HrAbsService {

	public BusinessHrService(Office office) {
		super(office);
	}

	@Override
	public void recruit() {
		System.out.println(super.office.getName() + "，招聘");
	}

	@Override
	public void dimission() {
		System.out.println(super.office.getName() + "，有员工离职");
	}

}
