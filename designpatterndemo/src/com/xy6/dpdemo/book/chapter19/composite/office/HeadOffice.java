package com.xy6.dpdemo.book.chapter19.composite.office;

import java.util.List;

import com.xy6.dpdemo.book.chapter19.composite.dept.finance.FinanceDept;
import com.xy6.dpdemo.book.chapter19.composite.dept.hr.HrDept;

/**
 * 总公司
 * 
 * @author zhang
 *
 */
public class HeadOffice extends Office {

	private String name;
	
	private HrDept hr;
	
	private FinanceDept finance;
	
	private List<BranchOffice> listBranch;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HrDept getHr() {
		return hr;
	}

	public void setHr(HrDept hr) {
		this.hr = hr;
	}

	public FinanceDept getFinance() {
		return finance;
	}

	public void setFinance(FinanceDept finance) {
		this.finance = finance;
	}

	public List<BranchOffice> getListBranch() {
		return listBranch;
	}

	public void setListBranch(List<BranchOffice> listBranch) {
		this.listBranch = listBranch;
	}

	@Override
	public String toString() {
		return "HeadOffice [name=" + name + ", hr=" + hr + ", finance=" + finance + ", listBranch=" + listBranch + "]";
	}

}
