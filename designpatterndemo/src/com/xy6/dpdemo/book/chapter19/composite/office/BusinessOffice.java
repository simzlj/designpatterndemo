package com.xy6.dpdemo.book.chapter19.composite.office;

import com.xy6.dpdemo.book.chapter19.composite.dept.finance.FinanceDept;
import com.xy6.dpdemo.book.chapter19.composite.dept.hr.HrDept;

/**
 * 办事处
 * 
 * @author zhang
 *
 */
public class BusinessOffice extends Office {

	private String name;
	
	private HrDept hr;
	
	private FinanceDept finance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HrDept getHr() {
		return hr;
	}

	public void setHr(HrDept hr) {
		this.hr = hr;
	}

	public FinanceDept getFinance() {
		return finance;
	}

	public void setFinance(FinanceDept finance) {
		this.finance = finance;
	}

	@Override
	public String toString() {
		return "BusinessOffice [name=" + name + ", hr=" + hr + ", finance=" + finance + "]";
	}
	
}
