package com.xy6.dpdemo.book.chapter19.composite;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 公司
 * <p>可能为总公司、分公司、办事处
 * 
 * @author zhang
 *
 */
public class ConcreteCompany extends Company {

	private List<Company> children = new LinkedList<>();

	public ConcreteCompany(String name) {
		super(name);
	}
	
	@Override
	public void add(Company com) {
		children.add(com);
	}

	@Override
	public void remove(Company com) {
		children.remove(com);
	}

	@Override
	public void display(int depth) {
		System.out.println(StringUtils.repeat("-", depth) + super.name);
		for(Company elem : children){
			elem.display(depth + 2);
		}
	}

	@Override
	public void duty() {
		for(Company elem : children){
			elem.duty();
		}
	}

}
