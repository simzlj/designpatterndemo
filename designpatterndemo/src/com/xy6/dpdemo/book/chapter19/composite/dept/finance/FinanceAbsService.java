package com.xy6.dpdemo.book.chapter19.composite.dept.finance;

import com.xy6.dpdemo.book.chapter19.composite.office.Office;

public abstract class FinanceAbsService {

	Office office;
	
	public FinanceAbsService(Office office){
		this.office = office;
	}
	
	/**
	 * 做预算
	 */
	public void budget(){
	}
	
	/**
	 * 报销
	 */
	public void expense(){
	}
	
}
