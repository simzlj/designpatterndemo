package com.xy6.dpdemo.book.chapter19.composite.dept.finance;

import com.xy6.dpdemo.book.chapter19.composite.office.Office;

public class HeadFinanceService extends FinanceAbsService {

	public HeadFinanceService(Office office) {
		super(office);
	}

	@Override
	public void budget() {
		System.out.println(super.office.getName() + "，财务部，做年度预算");
	}

	@Override
	public void expense() {
		System.out.println(super.office.getName() + "，财务部，报销差旅费");
	}

}
