package com.xy6.dpdemo.book.chapter19.composite.dept.hr;

/**
 * 人力资源部门
 * 
 * @author zhang
 *
 */
public class HrDept {

	private String name;
	
	private HrAbsService hrService;

	public HrDept(){
	}
	
	public HrDept(String name){
		this.name = name;
	}
	
	public HrDept(String name, HrAbsService hrService){
		this.name = name;
		this.hrService = hrService;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HrAbsService getHrService() {
		return hrService;
	}

	public void setHrService(HrAbsService hrService) {
		this.hrService = hrService;
	}
	
	public void recruit(){
		hrService.recruit();
	}
	
	public void dimission(){
		hrService.dimission();
	}
}
