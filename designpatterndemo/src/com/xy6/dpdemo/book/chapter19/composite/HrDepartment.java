package com.xy6.dpdemo.book.chapter19.composite;

import org.apache.commons.lang3.StringUtils;

/**
 * 人力资源部门
 * 
 * @author zhang
 *
 */
public class HrDepartment extends Company {

	public HrDepartment(String name) {
		super(name);
	}

	@Override
	public void add(Company com) {
	}

	@Override
	public void remove(Company com) {
	}

	@Override
	public void display(int depth) {
		System.out.println(StringUtils.repeat("-", depth) + super.name);
	}

	@Override
	public void duty() {
		System.out.println(super.name + "	员工招聘管理培训");
	}

}
