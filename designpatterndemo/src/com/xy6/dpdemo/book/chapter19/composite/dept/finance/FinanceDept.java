package com.xy6.dpdemo.book.chapter19.composite.dept.finance;

/**
 * 财务部
 * 
 * @author zhang
 *
 */
public class FinanceDept {
	
	private String name;
	
	private FinanceAbsService financeService;

	public FinanceDept(){
	}
	
	public FinanceDept(String name){
		this.name = name;
	}
	
	public FinanceDept(String name, FinanceAbsService financeService){
		this.name = name;
		this.financeService = financeService;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FinanceAbsService getFinanceService() {
		return financeService;
	}

	public void setFinanceService(FinanceAbsService financeService) {
		this.financeService = financeService;
	}
	
	
	public void budget(){
		financeService.budget();
	}
	
	public void expense(){
		financeService.expense();
	}
	
}
