package com.xy6.dpdemo.book.chapter19.composite.dept.hr;

import com.xy6.dpdemo.book.chapter19.composite.office.Office;

public abstract class HrAbsService {

	Office office;
	
	public HrAbsService(Office office){
		this.office = office;
	}
	
	/**
	 * 招聘
	 */
	public abstract void recruit();
	
	/**
	 * 离职
	 */
	public abstract void dimission();
	
}
