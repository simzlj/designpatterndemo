package com.xy6.dpdemo.book.chapter19.composite;

/**
 * 抽象公司bean
 * 
 * @author zhang
 *
 */
public abstract class Company {

	protected String name;

	public Company(String name){
		this.name = name;
	}
	
	public void add(Company com){
	}
	
	public void remove(Company com){
	}
	
	public void display(int depth){
	}
	
	public void duty(){
	}
	
}
