package com.xy6.dpdemo.book.chapter19.composite.office;

import java.util.List;

import com.xy6.dpdemo.book.chapter19.composite.dept.finance.FinanceDept;
import com.xy6.dpdemo.book.chapter19.composite.dept.hr.HrDept;

/**
 * 分公司
 * 
 * @author zhang
 *
 */
public class BranchOffice extends Office {


	private String name;
	
	private HrDept hr;
	
	private FinanceDept finance;
	
	private List<BusinessOffice> listBiz;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HrDept getHr() {
		return hr;
	}

	public void setHr(HrDept hr) {
		this.hr = hr;
	}

	public FinanceDept getFinance() {
		return finance;
	}

	public void setFinance(FinanceDept finance) {
		this.finance = finance;
	}

	public List<BusinessOffice> getListBiz() {
		return listBiz;
	}

	public void setListBiz(List<BusinessOffice> listBiz) {
		this.listBiz = listBiz;
	}

	@Override
	public String toString() {
		return "BranchOffice [name=" + name + ", hr=" + hr + ", finance=" + finance + ", listBiz=" + listBiz + "]";
	}
	
}
