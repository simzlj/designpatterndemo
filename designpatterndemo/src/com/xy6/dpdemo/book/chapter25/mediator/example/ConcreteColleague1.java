package com.xy6.dpdemo.book.chapter25.mediator.example;

/**
 * 具体同事对象
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class ConcreteColleague1 extends Colleague {

	public ConcreteColleague1(Mediator m) {
		super(m);
	}
	
	public void send(String message){
		mediator.send(message, this);
	}
	
	public void Notify(String message){
		System.out.println("同事1收到消息，" + message);
	}

}
