package com.xy6.dpdemo.book.chapter25.mediator.un;

import com.xy6.dpdemo.book.chapter25.mediator.un.country.America;
import com.xy6.dpdemo.book.chapter25.mediator.un.country.Iraq;

/**
 * 联合国，安理会
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class SecurityCouncil extends UN {

	private America america;
	private Iraq iraq;
	
	public America getAmerica() {
		return america;
	}

	public void setAmerica(America america) {
		this.america = america;
	}

	public Iraq getIraq() {
		return iraq;
	}

	public void setIraq(Iraq iraq) {
		this.iraq = iraq;
	}

	@Override
	public void send(String message, Country target) {
		if(this.america == target){
			this.america.recMsg(message);
		} else if(this.iraq == target){
			this.iraq.recMsg(message);
		}
	}

}
