package com.xy6.dpdemo.book.chapter25.mediator.un.country;

import com.xy6.dpdemo.book.chapter25.mediator.un.Country;
import com.xy6.dpdemo.book.chapter25.mediator.un.UN;

/**
 * 美国
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class America extends Country {

	public America(UN un) {
		super(un);
	}

	@Override
	public void send(String msg, Country target) {
		un.send(msg, target);
	}
	
	public void recMsg(String message){
		System.out.println("America receive msg:" + message);
	}

}
