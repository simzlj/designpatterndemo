package com.xy6.dpdemo.book.chapter25.mediator.un.country;

import com.xy6.dpdemo.book.chapter25.mediator.un.Country;
import com.xy6.dpdemo.book.chapter25.mediator.un.UN;

/**
 * 伊拉克
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class Iraq extends Country {

	public Iraq(UN un) {
		super(un);
	}

	@Override
	public void send(String msg, Country target) {
		un.send(msg, target);
	}
	
	public void recMsg(String message){
		System.out.println("Iraq receive msg:" + message);
	}

}
