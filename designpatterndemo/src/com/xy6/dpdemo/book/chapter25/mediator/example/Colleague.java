package com.xy6.dpdemo.book.chapter25.mediator.example;

/**
 * 同事接口
 * <p>依赖中介者，因为同事间沟通通过中介者进行
 * 
 * @author zhang
 * @since 2017-07-11
 */
public abstract class Colleague {

	protected Mediator mediator;
	
	public Colleague(Mediator m){
		this.mediator = m;
	}
	
}
