package com.xy6.dpdemo.book.chapter25.mediator.un;

/**
 * 联合国接口
 * 
 * @author zhang
 * @since 2017-07-11
 */
public abstract class UN {

	public abstract void send(String message, Country target);
	
}
