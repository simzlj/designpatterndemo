package com.xy6.dpdemo.book.chapter25.mediator.example;

/**
 * 中介者接口
 * <p>定义所有用户发生的行为
 * 
 * @author zhang
 * @since 2017-07-11
 */
public abstract class Mediator {

	public abstract void send(String message, Colleague c);
	
}
