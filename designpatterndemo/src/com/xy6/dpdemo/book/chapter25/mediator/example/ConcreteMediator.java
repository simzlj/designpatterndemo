package com.xy6.dpdemo.book.chapter25.mediator.example;

/**
 * 中介者具体类
 * <p>定义所有需要协调的对象，及对象发生的行为
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class ConcreteMediator extends Mediator {

	private ConcreteColleague1 c1;
	private ConcreteColleague2 c2;
	
	public ConcreteColleague1 getC1() {
		return c1;
	}

	public void setC1(ConcreteColleague1 c1) {
		this.c1 = c1;
	}

	public ConcreteColleague2 getC2() {
		return c2;
	}

	public void setC2(ConcreteColleague2 c2) {
		this.c2 = c2;
	}

	@Override
	public void send(String message, Colleague c) {
		if(c == c1){
			c2.Notify(message);
		} else {
			c1.Notify(message);
		}
	}

}
