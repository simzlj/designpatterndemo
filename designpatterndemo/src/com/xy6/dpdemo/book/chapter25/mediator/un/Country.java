package com.xy6.dpdemo.book.chapter25.mediator.un;

/**
 * 国家接口
 * 
 * @author zhang
 * @since 2017-07-11
 */
public abstract class Country {

	protected UN un;
	
	public Country(UN un){
		this.un = un;
	}
	
	public abstract void send(String msg, Country target);
	
	public void recMsg(String message){
	}
	
}
