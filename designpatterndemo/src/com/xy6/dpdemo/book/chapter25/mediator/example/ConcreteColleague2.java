package com.xy6.dpdemo.book.chapter25.mediator.example;

/**
 * 具体同事对象
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class ConcreteColleague2 extends Colleague {

	public ConcreteColleague2(Mediator m) {
		super(m);
	}
	
	public void send(String message){
		mediator.send(message, this);
	}
	
	public void Notify(String message){
		System.out.println("同事2收到消息，" + message);
	}

}
