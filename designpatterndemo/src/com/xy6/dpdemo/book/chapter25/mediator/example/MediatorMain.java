package com.xy6.dpdemo.book.chapter25.mediator.example;

/**
 * 设计一个程序，模拟同事间相互发消息
 * <p>可能会有多个同事
 * 
 * <pre>
 * 多个对象间互发消息，为了实现对象间解耦，创建一个中介者。将消息等信息发给中介者，中介者负责将消息转发给其他对象。
 * 中介者模式。
 * 用一个中介对象来封装一系列的对象交互。中介者使各对象不需要显示地相互引用，从而使其耦合松散，而且可以独立地改变
 * 他们之间的交互。
 * 缺点：由于ConcreteMediator控制了集中化，于是所有交互复杂性都变成了中介者的复杂性，这就是使中介者变得比任意
 * 一个ConcreteColleague都复杂。
 * 应用场景：一组对象已定义良好，但是以复杂方式进行通信的场景。
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class MediatorMain {

	public static void main(String[] args) {
		ConcreteMediator m = new ConcreteMediator();
		ConcreteColleague1 c1 = new ConcreteColleague1(m); 
		ConcreteColleague2 c2 = new ConcreteColleague2(m); 
		
		m.setC1(c1);
		m.setC2(c2);
		
		c1.send("吃饭了吗？");
		c2.send("没呢，你请客？");
	}

}
