package com.xy6.dpdemo.book.chapter25.mediator.un;

import com.xy6.dpdemo.book.chapter25.mediator.un.country.America;
import com.xy6.dpdemo.book.chapter25.mediator.un.country.Iraq;

/**
 * 设计一个程序，模拟美国和伊拉克通过联合国安理会进行对话
 * <pre>
 * 安理会是联合国的一个机构，还有国际教科文组织、世界贸易组织等
 * 
 * 中介者/调停者模式，用一个对象来封装一系列的对象交互。中介者使各对象不需要显示地相互引入，
 * 从而使其耦合松散，而且可以独立地改变他们之间的交互。
 * 由于ConcreteMediator控制了集中化，于是就把交互复杂性变为了中介者的复杂性，这就使得
 * 中介者比任何一个ConcreteColleagure都复杂。
 * 中介者模式一般应用于一组结构简单的对象以复杂方式进行通信的场合。
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-11
 */
public class UNMain {

	public static void main(String[] args) {
		SecurityCouncil un = new SecurityCouncil();
		America america = new America(un);
		Iraq iraq = new Iraq(un);
		
		un.setAmerica(america);
		un.setIraq(iraq);
		
		america.send("give me your oil", iraq);
		iraq.send("no", america);
		america.send("ok, one dollar per gallon", iraq);
		iraq.send("go away", america);
		america.send("u are died !", iraq);
	}

}
