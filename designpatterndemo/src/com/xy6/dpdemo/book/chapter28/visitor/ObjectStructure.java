package com.xy6.dpdemo.book.chapter28.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * 对象结构，封装对数据结构、操作的访问
 * 
 * @author zhang
 * @since 2017-07-20
 */
public class ObjectStructure {

	private List<Human> humans = new ArrayList<>();
	
	public void add(Human h){
		humans.add(h);
	}
	
	public void remove(Human h){
		humans.remove(h);
	}
	
	/**
	 * 双分派技术
	 * 
	 * <pre>
	 * 在客户端将具体状态作为参数传递给man类，完成第一次分派；
	 * man类调用作为参数的“具体状态”中的方法manReflect，同时将自己作为参数传递进去，完成第二次分派。
	 * 双分派意味着操作取决于请求的种类和两个接收者的类型。
	 * </pre>
	 */
	public void display(State state){
		for(Human elem : humans){
			elem.reflect(state);
		}
	}
	
}
