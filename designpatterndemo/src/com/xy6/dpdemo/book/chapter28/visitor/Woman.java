package com.xy6.dpdemo.book.chapter28.visitor;

/**
 * 女人，具体类
 * 
 * @author zhang
 * @since 2017-07-20
 */
public class Woman extends Human {

	public Woman(){
		super.setName("女人");
	}

	@Override
	public void reflect(State s) {
		s.womanReflect(this);
	}

}
