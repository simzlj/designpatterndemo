package com.xy6.dpdemo.book.chapter28.visitor;

/**
 * 男人，具体类
 * 
 * @author zhang
 * @since 2017-07-20
 */
public class Man extends Human {

	public Man(){
		super.setName("男人");
	}
	
	@Override
	public void reflect(State s) {
		s.manReflect(this);
	}

}
