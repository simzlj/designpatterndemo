package com.xy6.dpdemo.book.chapter28.visitor.person;

public class MyMan extends Person {

	@Override
	public void reflect() {
		if("成功".equals(action)){
			System.out.println("男人成功时，背后多半有一个伟大的女人。");
		} else if("失败".equals(action)){
			System.out.println("男人失败时，闷头喝酒，谁也不用劝。");
		} else if("恋爱".equals(action)){
			System.out.println("男人恋爱时，凡事不懂也装懂。");
		}
	}

}
