package com.xy6.dpdemo.book.chapter28.visitor;

/**
 * 人接口
 * 
 * @author zhang
 * @since 2017-07-20
 */
public abstract class Human {
	
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract void reflect(State s);
	
}
