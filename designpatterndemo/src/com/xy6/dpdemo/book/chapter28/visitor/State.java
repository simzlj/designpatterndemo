package com.xy6.dpdemo.book.chapter28.visitor;

/**
 * 状态接口
 * 
 * @author zhang
 * @since 2017-07-20
 */
public abstract class State {

	protected String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public abstract void manReflect(Human human);
	
	public abstract void womanReflect(Human human);
	
}
