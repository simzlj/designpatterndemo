package com.xy6.dpdemo.book.chapter28.visitor;

import com.xy6.dpdemo.book.chapter28.visitor.person.MyMan;
import com.xy6.dpdemo.book.chapter28.visitor.person.MyWoman;

/**
 * 设计一个程序，模拟男人、女人对多种事情的不同反应
 * 
 * <pre>
 * 定义一个事情的接口，把所有事情定义在接口中，在man、woman类中实现接口。
 * 缺点：新增一件事，需要修改接口、实现类、客户端，违反开放-封闭原则。
 * 改：男人、女人两类人比较稳定，多种事情，事情是不稳定的，未来可能扩展。
 * 创建Human接口，定义反应方法，添加man、woman两个子类。
 * 创建事情接口，定义男人反应、女人反应方法，添加具体事情实现类。新增一件事情，则新增一个实现类，接口无需修改。
 * 
 * 访问者模式，表示一个作用与某对象结构中的各元素的操作。它使你可以在不改变各元素的类的前提下定义
 * 作用于这些元素的新操作。
 * 目的：把处理从数据结构中分离出来。
 * 访问者模式适用于数据结构相对稳定的系统。
 * 优点：增加新的操作很容易，缺点：增加新的数据结构很困难。
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-20
 */
public class HumanMain {

	public static void main(String[] args) {
		test3();
	}
	
	public static void test3(){
		State success = new StateSuccess();
		State fail = new StateFail();
		
		Man man = new Man();
		Woman woman = new Woman();
		ObjectStructure struct = new ObjectStructure();
		struct.add(woman);
		struct.add(man);
		
		struct.display(success);
		struct.display(fail);
	}
	
	public static void test2(){
		MyMan man = new MyMan();
		MyWoman woman = new MyWoman();
		
		man.setAction("成功");
		man.reflect();
		woman.setAction("成功");
		woman.reflect();
		
		man.setAction("失败");
		man.reflect();
		woman.setAction("失败");
		woman.reflect();
		
		man.setAction("恋爱");
		man.reflect();
		woman.setAction("恋爱");
		woman.reflect();
	}

}
