package com.xy6.dpdemo.book.chapter28.visitor;

/**
 * 失恋，具体状态
 * 
 * @author zhang
 * @since 2017-07-20
 */
public class StateFail extends State {

	public StateFail(){
		super.setType("失败");
	}
	
	@Override
	public void manReflect(Human human) {
		System.out.println(String.format("%s%s时，闷头喝酒，谁也不用劝。", human.getName(), this.getType()));
	}

	@Override
	public void womanReflect(Human human) {
		System.out.println("女人失败时，眼泪汪汪，谁也劝不了。");
	}

}
