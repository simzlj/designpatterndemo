package com.xy6.dpdemo.book.chapter28.visitor.person;

public class MyWoman extends Person {

	@Override
	public void reflect() {
		if("成功".equals(action)){
			System.out.println("女人成功时，背后大多有一个不成功的男人。");
		} else if("失败".equals(action)){
			System.out.println("女人失败时，眼泪汪汪，谁也劝不了。");
		} else if("恋爱".equals(action)){
			System.out.println("女人恋爱时，遇事懂也装不懂。");
		}
	}

}
