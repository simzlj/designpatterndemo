package com.xy6.dpdemo.book.chapter28.visitor;

/**
 * 成功，具体状态
 * 
 * @author zhang
 * @since 2017-07-20
 */
public class StateSuccess extends State {

	public StateSuccess(){
		super.setType("成功");
	}

	@Override
	public void manReflect(Human human) {
		System.out.println("男人成功时，背后多半有一个伟大的女人。");
	}

	@Override
	public void womanReflect(Human human) {
		System.out.println("女人成功时，背后大多有一个不成功的男人。");
	}

}
