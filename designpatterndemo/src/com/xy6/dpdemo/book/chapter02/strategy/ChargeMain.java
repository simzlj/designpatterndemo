package com.xy6.dpdemo.book.chapter02.strategy;

import com.xy6.dpdemo.book.chapter02.strategy.service.CashContext;

/**
 * 设计一个超市收银系统
 * <p>输入商品单价、数量，输出总价，并支持打折、满返等优惠活动
 * 
 * <pre>
 * 设计演变过程：
 * 在一个类中实现根据商品单价、数量，计算总价，并输出。
 * 改：添加打折、满返活动。添加一个工具类，提供打折、满返方法，计算优惠后的金额。缺点：各优惠活动耦合在一起
 * 改：使用工厂模式改造，将优惠活动封装到一个个bean中
 * 改：使用工厂+策略模式改造，工厂负责生产不同的优惠活动bean，策略模式负责根据不同优惠活动，计算优惠后的结果。
 * 客户端只需要依赖策略模式开放的类，无需依赖工厂类及工厂生产的具体策略bean
 * 
 * 策略模式：定义了算法家族，分别封装起来，让它们之间可以相互替换。此模式让算法的变化，不影响客户端。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-11
 */
public class ChargeMain {

	public static void main(String[] args) {
		if(null == args || args.length < 3){
			System.err.println("请输入商品单价、数量、优惠活动编号");
			return;
		}
		// 解析入参
		double price = Double.valueOf(args[0]);
		double num = Double.valueOf(args[1]);
		int type = Integer.valueOf(args[2]);
		
		// 调用strategy模式上下文类
		CashContext context = new CashContext(type);
		double result = context.getResult(price * num);
		
		System.err.println("total money is:" + result);
	}

}
