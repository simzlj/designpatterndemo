package com.xy6.dpdemo.book.chapter02.strategy.service;

/**
 * 优惠活动具体策略，满返情况
 * <p>strategy模式中的strategy层
 * 
 * @author zhang
 * @since 2017-06-11
 */
public class CashReturnStrategy extends CashStrategySuper {

	/**
	 * 满XX元
	 */
	private double moneyCondition = 0d;
	
	/**
	 * 返XX元
	 */
	private double moneyReturn = 0d;

	public CashReturnStrategy(double moneyCondition, double moneyReturn){
		this.moneyCondition = moneyCondition;
		this.moneyReturn = moneyReturn;
	}
	
	@Override
	public double acceptCash(double money) {
		if(money < moneyCondition){
			return money;
		}
		return money - Math.floor(money / moneyCondition) * moneyReturn;
	}

}
