package com.xy6.dpdemo.book.chapter02.strategy.service;

/**
 * 优惠活动具体策略的父类
 * <p>各优惠活动有一个共同点，输入原价，输出优惠后的价格
 * 
 * @author zhang
 * @since 2017-06-11
 */
public abstract class CashStrategySuper {

	/**
	 * 优惠后的金额
	 * 
	 * @param money 优惠前的金额
	 * @return
	 */
	public abstract double acceptCash(double money);
	
}
