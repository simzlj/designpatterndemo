package com.xy6.dpdemo.book.chapter02.strategy.service;

/**
 * 优惠活动工厂类
 * <p>工厂模式，生产优惠活动bean
 * 
 * @author zhang
 * @since 2017-06-11
 */
public class CashFactory {

	/**
	 * 根据不同类型，生产不同优惠活动bean
	 * 
	 * @param type
	 * @return
	 */
	public static CashStrategySuper getCash(int type){
		switch(type){
			case 1:
				return new CashNormalStrategy();
			case 2:
				return new CashRebateStrategy(0.8);
			case 3:
				return new CashReturnStrategy(100, 20);
			default:
				return new CashNormalStrategy();
		}
	}
	
}
