package com.xy6.dpdemo.book.chapter02.strategy.service;

/**
 * 优惠活动具体策略，打折情况
 * <p>strategy模式中的strategy层
 * 
 * @author zhang
 * @since 2017-06-11
 */
public class CashRebateStrategy extends CashStrategySuper {

	/**
	 * 打折率，默认为1，即不打折
	 */
	private double rebate = 1d;
	
	public CashRebateStrategy(double rebate){
		this.rebate = rebate;
	}
	
	@Override
	public double acceptCash(double money) {
		return this.rebate * money;
	}

}
