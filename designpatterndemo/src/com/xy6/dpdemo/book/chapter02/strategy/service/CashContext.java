package com.xy6.dpdemo.book.chapter02.strategy.service;

/**
 * 策略模式上下文
 * <p>对调用者开放的类
 * 
 * @author zhang
 * @since 2017-06-11
 */
public class CashContext {

	CashStrategySuper cash;
	
	/**
	 * 初始化时，传入具体的策略对象
	 * 
	 * @param type
	 */
	public CashContext(int type){
		// 传入不同活动类型，使用不同的活动策略初始化
		// 封装实例化具体策略的过程
		this.cash = CashFactory.getCash(type);
	}
	
	/**
	 * 计算优惠后的金额
	 * <p>根据具体的策略对象，调用其算法的方法
	 * 
	 * @param money
	 * @return
	 */
	public double getResult(double money){
		return cash.acceptCash(money);
	}
	
}
