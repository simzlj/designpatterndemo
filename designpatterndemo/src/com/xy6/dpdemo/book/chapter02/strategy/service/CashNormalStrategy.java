package com.xy6.dpdemo.book.chapter02.strategy.service;

/**
 * 优惠活动具体策略，正常情况
 * <p>strategy模式中的strategy层
 * 
 * @author zhang
 * @since 2017-06-11
 */
public class CashNormalStrategy extends CashStrategySuper {

	@Override
	public double acceptCash(double money) {
		return money;
	}

}
