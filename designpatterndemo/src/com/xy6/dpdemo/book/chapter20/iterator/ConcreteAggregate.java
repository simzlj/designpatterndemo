package com.xy6.dpdemo.book.chapter20.iterator;

/**
 * 聚集类
 * <p>创建一个集合
 * 
 * @author zhang
 * @since 2017-06-29
 */
public class ConcreteAggregate extends Aggregate {

	@Override
	public Iterator createIterator() {
		return new ConcreteIterator();
	}

}
