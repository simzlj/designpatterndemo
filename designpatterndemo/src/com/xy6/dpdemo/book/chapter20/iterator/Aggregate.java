package com.xy6.dpdemo.book.chapter20.iterator;

/**
 * 聚集抽象类
 * 
 * @author zhang
 * @since 2017-06-29
 */
public abstract class Aggregate {

	public abstract Iterator createIterator();
	
}
