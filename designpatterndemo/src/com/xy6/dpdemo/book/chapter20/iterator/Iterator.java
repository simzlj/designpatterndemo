package com.xy6.dpdemo.book.chapter20.iterator;

/**
 * 抽象集合类
 * <p>定义集合应有的各种行为，新增、删除、上一个、下一个等
 * 
 * @author zhang
 * @since 2017-06-29
 */
public abstract class Iterator {

	/**
	 * 上一个元素
	 * 
	 * @return
	 */
	public abstract Object pre();
	
	/**
	 * 下一个元素
	 * 
	 * @return
	 */
	public abstract Object next();
	
	/**
	 * 是否有下一个元素
	 * 
	 * @return
	 */
	public abstract boolean hasNext();
	
	/**
	 * 当前元素
	 * 
	 * @return
	 */
	public abstract Object cur();
	
	/**
	 * 新增元素
	 * 
	 * @param o
	 * @return
	 */
	public abstract boolean add(Object o);
	
	/**
	 * 删除元素
	 * <p>删除第一个匹配的元素，而非所有匹配的元素
	 * 
	 * @param o
	 * @return
	 */
	public abstract boolean remove(Object o);
	
}
