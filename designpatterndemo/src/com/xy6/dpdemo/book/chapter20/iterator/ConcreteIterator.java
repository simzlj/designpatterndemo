package com.xy6.dpdemo.book.chapter20.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 集合类
 * 
 * @author zhang
 * @since 2017-06-29
 */
public class ConcreteIterator extends Iterator {

	/**
	 * 元素集合
	 */
	private List<Object> elems = new ArrayList<>();

	/**
	 * 当前位置
	 */
	private int curIndex = -1;

	@Override
	public Object pre() {
		if (elems.isEmpty()) {
			return null;
		}
		if (curIndex <= 0) {
			return null;
		}
		curIndex--;
		return elems.get(curIndex);
	}

	@Override
	public Object next() {
		if (elems.isEmpty()) {
			return null;
		}
		if (curIndex == elems.size() - 1) {
			return null;
		}
		curIndex++;
		return elems.get(curIndex);
	}

	@Override
	public boolean hasNext() {
		if (elems.isEmpty()) {
			return false;
		}
		if (curIndex < elems.size() - 1) {
			return true;
		}
		return false;
	}

	@Override
	public Object cur() {
		if (elems.isEmpty()) {
			return null;
		}
		if(curIndex < 0){
			return null;
		}
		return elems.get(curIndex);
	}

	@Override
	public boolean add(Object o) {
		elems.add(o);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		elems.remove(o);
		return true;
	}

	@Override
	public String toString() {
		return "ConcreteIterator [elems=" + elems + "]";
	}

}
