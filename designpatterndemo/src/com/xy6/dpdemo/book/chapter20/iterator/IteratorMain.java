package com.xy6.dpdemo.book.chapter20.iterator;

/**
 * 设计一个迭代器，可以新增、删除元素，取前一个、后一个元素
 * 
 * <p>实现jdk中List或Stack的功能
 * <p>迭代器模式，提供一种方法顺序访问聚合对象中的各个元素，而又不暴露该对象的内部表示。
 * 
 * @author zhang
 * @since 2017-06-29
 */
public class IteratorMain {

	public static void main(String[] args){
		Aggregate agg = new ConcreteAggregate();
		Iterator iter = agg.createIterator();
		iter.add("a");
		iter.add("b");
		iter.add("c");
		System.out.println(iter);
		
		while(iter.hasNext()){
			System.out.println(iter.next());
		}
		iter.pre();
		
		iter.remove("a");
		System.out.println(iter);
	}
	
}
