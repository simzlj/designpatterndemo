package com.xy6.dpdemo.book.chapter21.singleton;

/**
 * 设计一个单例类
 * <p>工程提供了4种实现单例类的方式，其中一些方式存在缺陷，且通过一些写法可以重现其缺陷。
 * <p>最终推荐使用内部类实现单例模式
 * <p>单例模式：保证一个类只有一个实例，并提供一个访问它的全局访问点。
 * 
 * @author zhang
 * @since 2017-06-30
 */
public class ClientMain {

	public static void main(String[] args) {
		run4();
	}
	
	@SuppressWarnings("unused")
	private static void run1(){
		int num = 10;
		for(int i=0; i<num; i++){
			Thread t = new Thread(new Thread1(), "thread-" + i);
			t.start();
		}
	}
	
	@SuppressWarnings("unused")
	private static void run2(){
		int num = 10;
		for(int i=0; i<num; i++){
			Thread t = new Thread(new Thread2(), "thread-" + i);
			t.start();
		}
	}
	
	@SuppressWarnings("unused")
	private static void run3(){
		int num = 10;
		for(int i=0; i<num; i++){
			Thread t = new Thread(new Thread3(), "thread-" + i);
			t.start();
		}
	}
	
	private static void run4(){
		int num = 10;
		for(int i=0; i<num; i++){
			Thread t = new Thread(new Thread4(), "thread-" + i);
			t.start();
		}
	}

}
