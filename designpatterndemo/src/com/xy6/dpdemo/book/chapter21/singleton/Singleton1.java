package com.xy6.dpdemo.book.chapter21.singleton;

/**
 * 单例类1
 * <p>获取实例时，判空。如果为空，则创建一个实例。
 * <p>缺点：并发环境下，多个线程同时获取实例，对象为空，都进入创建实例代码，导致创建了多个实例，错误。
 * 
 * @author zhang
 * @since 2017-06-30
 */
public class Singleton1 {

	private static Singleton1 instance = null;
	
	private static Long time = 0L;
	
	/**
	 * 初始化
	 * <p>加睡眠，模拟初始化耗时很久
	 */
	private Singleton1(){
		System.out.println("begin init...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		time++;
		System.out.println("end init..." + time);
	}
	
	public static Singleton1 getInstance(){
		if(null == instance){
			instance = new Singleton1();
		}
		return instance;
	}
	
	public void show(){
		System.out.println(time);
	}
	
}
