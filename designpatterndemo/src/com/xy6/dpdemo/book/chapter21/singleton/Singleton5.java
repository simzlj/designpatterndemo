package com.xy6.dpdemo.book.chapter21.singleton;

/**
 * 单例类3
 * <pre>
 * volatile的作用：
 * 1.保证可见性；
 * 	可以保证在多线程环境下，变量的修改可见性。
 * 2.提供内存屏障
 * 	volatile关键字能够通过提供内存屏障，来保证某些指令顺序处理器不能够优化重排，
 * 	编译器在生成字节码时，会在指令序列中插入内存屏障来禁止特定类型的处理器重排序。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-30
 */
public class Singleton5 {

	private static volatile Singleton5 instance = null;

	private static Long time = 0L;

	/**
	 * 初始化
	 * <p>
	 * 加睡眠，模拟初始化耗时很久
	 */
	private Singleton5() {
		System.out.println("begin init...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		time++;
		System.out.println("end init..." + time);
	}

	/**
	 * 获取实例
	 * 
	 * <pre>
	 * 理论上，当A、B两个线程同时进入第一个判空时，有可能出现空指针一次，暂未能重现。
	 * </pre>
	 * 
	 * @return
	 */
	public static Singleton5 getInstance() {
		if(null == instance){
			synchronized(Singleton5.class) {
				if(null == instance){
					instance = new Singleton5();
				}
			}
		}
		return instance;
	}

	public void show() {
		System.out.println(time);
	}

}
