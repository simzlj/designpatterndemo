package com.xy6.dpdemo.book.chapter21.singleton;

/**
 * 单例类4
 * <p>
 * 使用内部类类维护单例的实现。
 * <p>
 * jvm内部机制能够保证当一个类加载时，这个类的加载过程是互斥的。
 * 
 * @author zhang
 * @since 2017-06-30
 */
public class Singleton4 {

	private static Singleton4 instance = null;

	private static Long time = 0L;

	/**
	 * 初始化
	 * <p>
	 * 加睡眠，模拟初始化耗时很久
	 */
	private Singleton4() {
		System.out.println("begin init...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		time++;
		System.out.println("end init..." + time);
	}

	/**
	 * 获取实例
	 * 
	 * <pre>
	 * 理论上，当A、B两个线程同时进入第一个判空时，有可能出现空指针一次，暂未能重现。
	 * </pre>
	 * 
	 * @return
	 */
	public static Singleton4 getInstance() {
		if (null == instance) {
			instance = Singleton4Factory.ins;
		}
		return instance;
	}

	public void show() {
		System.out.println(time);
	}
	
	
	private static class Singleton4Factory {
		private static Singleton4 ins = new Singleton4();
	}

}
