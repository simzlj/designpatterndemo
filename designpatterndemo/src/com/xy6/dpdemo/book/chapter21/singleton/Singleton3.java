package com.xy6.dpdemo.book.chapter21.singleton;

/**
 * 单例类3
 * <p>获取实例时，如果为空，对创建实例的过程加锁。
 * <p>缺点：并发环境下，可能报空指针。
 * 
 * @author zhang
 * @since 2017-06-30
 */
public class Singleton3 {

	private static Singleton3 instance = null;
	
	private static Long time = 0L;
	
	/**
	 * 初始化
	 * <p>加睡眠，模拟初始化耗时很久
	 */
	private Singleton3(){
		System.out.println("begin init...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		time++;
		System.out.println("end init..." + time);
	}
	
	/**
	 * 获取实例
	 * <pre>
	 * 理论上，当A、B两个线程同时进入第一个判空时，有可能出现空指针一次，暂未能重现。
	 * </pre>
	 * 
	 * @return
	 */
	public static Singleton3 getInstance(){
		
		// TODO 重现空指针异常。
		// new Singleton3()时，在内存中开辟一段空间指向instance，此时instance还是空的，
		// 如果此时访问instance的一些方法，会报空指针。
		
		if(null == instance){
			synchronized(Singleton3.class) {
				if(null == instance){
					instance = new Singleton3();
				}
			}
		}
		return instance;
	}
	
	public void show(){
		System.out.println(time);
	}
	
}
