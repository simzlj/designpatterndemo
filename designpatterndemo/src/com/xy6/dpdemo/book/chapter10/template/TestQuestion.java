package com.xy6.dpdemo.book.chapter10.template;

/**
 * 考试问题bean
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class TestQuestion {

	public void question1(){
		System.out.println("Q1:.......");
	}
	
	public void question2(){
		System.out.println("Q2:.......");
	}
	
	public void question3(){
		System.out.println("Q3:.......");
	}
	
}
