package com.xy6.dpdemo.book.chapter10.template;

/**
 * 设计一个考试答题程序，对同样问题，填写自己的答案
 * 
 * <pre>
 * 新建一个问题类，保存所有的问题。新建一个答题接口类，定义各答案的格式，子类实现接口，填写答案
 * 模板模式，把不变的行为移到超类，去除子类中的重复代码。
 * 
 * 模板方法模式，定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。
 * 模板方法使得可以子类可以不改变一个算法的结构，即可重定义算法的某些特定步骤。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class ExamMain {

	public static void main(String[] args) {
		TestQuestion q = new TestQuestion();
		
		TestAnswer answer1 = new TestAnswer1(q);
		answer1.answer1();
		answer1.answer2();
		answer1.answer3();
		
		System.out.println();
		
		TestAnswer answer2 = new TestAnswer2(q);
		answer2.answer1();
		answer2.answer2();
		answer2.answer3();
	}

}
