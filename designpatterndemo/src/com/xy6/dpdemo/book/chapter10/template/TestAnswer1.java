package com.xy6.dpdemo.book.chapter10.template;

/**
 * 具体答案bean
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class TestAnswer1 extends TestAnswer {

	public TestAnswer1(TestQuestion q) {
		super(q);
	}

	public void answer1(){
		q.question1();
		System.out.println("A");
	}
	
	public void answer2(){
		q.question2();
		System.out.println("B");
	}
	
	public void answer3(){
		q.question3();
		System.out.println("C");
	}

}
