package com.xy6.dpdemo.book.chapter10.template;

/**
 * 答案接口
 * <p>所有具体答案子类实现该接口，填写具体答案
 * 
 * @author zhang
 * @since 2017-06-19
 */
public abstract class TestAnswer {
	
	TestQuestion q;
	
	public TestAnswer(TestQuestion q){
		this.q = q;
	}

	public void answer1(){
	}
	
	public void answer2(){
	}
	
	public void answer3(){
	}
	
}
