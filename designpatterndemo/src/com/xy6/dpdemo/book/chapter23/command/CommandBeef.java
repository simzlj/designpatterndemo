package com.xy6.dpdemo.book.chapter23.command;

/**
 * 烤牛肉串命令类
 * 
 * @author zhang
 * @since 2017-07-07
 */
public class CommandBeef extends Command {

	public CommandBeef(Barbecuer bar) {
		super(bar);
	}

	@Override
	public void exec() {
		bar.makeBeefBunch(10);
	}

}
