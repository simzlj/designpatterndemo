package com.xy6.dpdemo.book.chapter23.command;

/**
 * 设计一个程序模拟烧烤店经营流程
 * <p>地摊：顾客直接告诉烧烤师傅所选菜，烧烤师傅做
 * <p>门店：顾客告诉服务器所选菜，服务员下单，烧烤师傅做
 * 
 * <pre>
 * 创建一个烧烤师傅类，定义并实现烤羊肉串、烤牛肉串等方法，客户端直接调用。
 * 缺点：紧耦合设计，违反开放-封闭原则。
 * 改：创建烧烤师傅类，定义并实现烤羊肉串、烤牛肉串等方法。创建命令接口类，定义执行方法。
 * 创建命令实现类，如烤羊肉命令类、烤牛肉命令类，实现具体烧烤方法。优点：客户端与烧烤师傅解耦，服务员担当
 * 命令接口类的角色，支持批量下单，撤单功能。
 * 开发阶段，一般不使用命令模式，在重构时才使用。它的提交命令、撤销命令功能，在一些场景中是不需要的，
 * 无需实现。
 * 
 * 命令模式：将一个请求封装为一个对象，从而实现可以用不同的请求对客户进行参数化。对请求排队或记录日志，
 * 从而支持可撤销的操作。
 * 命令模式最大优点：把请求一个操作的对象与执行一个操作的对象分离开
 * </pre>
 * 
 * @author zhang
 * @since 2017-07-07
 */
public class BarbecueMain {

	public static void main(String[] args) {
		test2();
	}
	
	@SuppressWarnings("unused")
	private static void test1(){
		Barbecuer bar = new Barbecuer();
		bar.makeSheepBunch(10);
		bar.makeVegetableBunch(5);
		
		Barbecuer bar2 = new Barbecuer();
		bar2.makeSheepBunch(20);
		bar2.makeBeefBunch(10);
		bar2.makeVegetableBunch(10);
	}
	
	private static void test2(){
		// 开店准备
		Barbecuer boy = new Barbecuer();
		Command sheep = new CommandSheep(boy);
		Command beef = new CommandBeef(boy);
		Waiter girl = new Waiter();
		
		// 顾客下单
		girl.addBill(sheep);
		girl.addBill(beef);
		girl.addBill(sheep);
		girl.addBill(sheep);
		girl.show();
		girl.deleteBill(sheep);
		girl.show();
		
		// 执行
		girl.Notify();
	}

}
