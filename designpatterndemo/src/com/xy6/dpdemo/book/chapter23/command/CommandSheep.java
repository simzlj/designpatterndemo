package com.xy6.dpdemo.book.chapter23.command;

/**
 * 烤羊肉串命令类
 * 
 * @author zhang
 * @since 2017-07-07
 */
public class CommandSheep extends Command {

	public CommandSheep(Barbecuer bar) {
		super(bar);
	}

	@Override
	public void exec() {
		bar.makeSheepBunch(10);
	}

}
