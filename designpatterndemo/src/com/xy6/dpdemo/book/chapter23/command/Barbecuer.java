package com.xy6.dpdemo.book.chapter23.command;

/**
 * 烧烤师傅类
 * <p>定义并实现各种烤串
 * 
 * @author zhang
 * @since 2017-07-07
 */
public class Barbecuer {

	public void makeSheepBunch(int num){
		System.out.println(String.format("烤%d个羊肉串", num));
	}
	
	public void makeBeefBunch(int num){
		System.out.println(String.format("烤%d个牛肉串", num));
	}
	
	public void makeVegetableBunch(int num){
		System.out.println(String.format("烤%d个青菜卷", num));
	}
	
}
