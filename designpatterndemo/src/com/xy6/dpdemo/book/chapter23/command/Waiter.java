package com.xy6.dpdemo.book.chapter23.command;

import java.util.LinkedList;
import java.util.List;

/**
 * 服务员类
 * <p>对客户端开放，支持下单、撤单
 * 
 * @author zhang
 * @since 2017-07-07
 */
public class Waiter {

	List<Command> comms = new LinkedList<>();
	
	public void addBill(Command c){
		comms.add(c);
	}
	
	public void Notify(){
		for(Command elem : comms){
			elem.exec();
		}
	}
	
	public boolean deleteBill(Command c){
		return comms.remove(c);
	}
	
	public void show(){
		System.out.println("已点的菜：");
		for(Command elem : comms){
			System.out.println(elem);
		}
	}
}
