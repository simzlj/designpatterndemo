package com.xy6.dpdemo.book.chapter23.command;

/**
 * 烧烤命令接口
 * <p>定义命令执行方法，但不实现
 * 
 * @author zhang
 * @since 2017-07-07
 */
public abstract class Command {
	
	protected Barbecuer bar;
	
	public Command(Barbecuer bar){
		this.bar = bar;
	}

	public abstract void exec();
	
}
