package com.xy6.dpdemo.book.delegate.task;

/**
 * 任务执行器接口
 * 
 * @author zhang
 * @since 2018-04-29
 */
public interface ITaskExecuter {

	public void run(String taskId);
	
	public void getTask(String taskId);
	
}
