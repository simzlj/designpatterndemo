package com.xy6.dpdemo.book.delegate.task;

/**
 * 任务执行器实现类。实现运行、查询任务
 * <pre>
 * 通过代理模式，实现在运行任务前、后记录日志
 * </pre>
 * 
 * @author zhang
 * @since 2018-04-29
 */
public class TaskExecuter implements ITaskExecuter {

	public void run(String taskId){
		System.out.println("run task " + taskId);
	}
	
	public void beforeRun(String taskId){
		System.out.println("before run");
	}
	
	public void afterRun(String taskId){
		System.out.println("after run");
	}
	
	public void getTask(String taskId){
		System.out.println("get task by id " + taskId);
	}
	
	public void deleteTask(String taskId){
		System.out.println("delete task by id " + taskId);
	}
	
	public static void main(String[] args){
		TaskExecuter taskObj = new TaskExecuter();
		taskObj.run("001");
		System.out.println("-----------");
		
		TaskExecuter taskObj2 = new TaskExecuter();
		ITaskExecuter task2 = new TaskExecuterDelegator().createProxy(taskObj2);
		task2.run("001");
		System.out.println("-----------");
		task2.getTask("001");
		System.out.println("-----------");
		System.out.println(task2);
		
		// 检查两次生成的代理类是否为同一个对象，toString()相同，但是不等，原因？
		ITaskExecuter task3 = new TaskExecuterDelegator().createProxy(taskObj2);
		System.out.println(task3);
		System.out.println(task2 == task3);
	}
	
}
