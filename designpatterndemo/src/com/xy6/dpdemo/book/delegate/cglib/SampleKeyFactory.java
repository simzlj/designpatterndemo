package com.xy6.dpdemo.book.delegate.cglib;

public interface SampleKeyFactory {

	Object newInstance(String first, int second);
	
}
