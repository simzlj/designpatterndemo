package com.xy6.dpdemo.book.delegate.cglib;

public interface BeanDelegate {

	String getValueFromDelegate();
	
}
