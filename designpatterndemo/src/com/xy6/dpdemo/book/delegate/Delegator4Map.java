package com.xy6.dpdemo.book.delegate;

import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Map;

/**
 * 一个委托的例子，实现Map的功能。
 * 
 * @author zhang
 * @since 2018-04-18
 */
@SuppressWarnings("rawtypes")
public class Delegator4Map extends Delegator {

	// 原始对象
	private Map orginClass = null;
	// 代理对象
	private Map proxyClass = null;
	
	public Map getOrgin(){
		return orginClass;
	}
	
	public Map getProxy(){
		return proxyClass;
	}
	
	public Delegator4Map(Map orgin){
		super(orgin);
		orginClass = orgin;
		proxyClass = (Map)super.obj_proxy;
	}
	
	@Override
	public Object invoke(Object obj, Method method, Object[] args) throws Throwable{
		// 修改size处理逻辑
		if(method.getName().equals("size")){
			Object res2 = new Integer(-1);
			System.out.println("调用委托的方法");
			return res2;
		} else {
			System.out.println("调用原始的方法");
			return super.invoke(obj, method, args);
		}
	}
	
	public static void main(String[] args) {
		Delegator4Map rtm = new Delegator4Map(new Hashtable());
		Map m = rtm.getProxy();
		m.size();
//		System.out.println(m.size());
//		System.out.println(m.toString());
//		System.out.println(m.isEmpty());
	}

}
