package com.xy6.dpdemo.book.chapter15.abstractfactory.user.service;

import com.xy6.dpdemo.book.chapter15.abstractfactory.user.User;

/**
 * 用户服务类，以sqlserver作为db
 * 
 * @author zhang
 * @since 2017-06-22
 */
public class SqlserverUserService implements IUserService {

	@Override
	public User find(String id) {
		System.out.println("get user, sql server");
		return null;
	}

}
