package com.xy6.dpdemo.book.chapter15.abstractfactory;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.AccessDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.IDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.AccessUserService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.IUserService;

/**
 * access数据库工厂类
 * <p>生产以access作为db的接口
 * 
 * @author zhang
 * @since 2017-06-22
 */
public class AccessFactory implements IFactory {

	@Override
	public IUserService createUserService() {
		return new AccessUserService();
	}

	@Override
	public IDeptService createDeptService() {
		return new AccessDeptService();
	}

}
