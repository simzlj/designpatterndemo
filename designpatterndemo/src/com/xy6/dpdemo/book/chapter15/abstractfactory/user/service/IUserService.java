package com.xy6.dpdemo.book.chapter15.abstractfactory.user.service;

import com.xy6.dpdemo.book.chapter15.abstractfactory.user.User;

/**
 * 用户接口
 * <p>定义用户服务类具有的行为
 * 
 * @author zhang
 * @since 2017-06-22
 */
public interface IUserService {

	public User find(String id);
	
}
