package com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.Department;

/**
 * 部门服务类，以access作为db
 * 
 * @author zhang
 * @since 2017-06-22
 */
public class AccessDeptService implements IDeptService {

	@Override
	public Department find(String id) {
		System.out.println("find dept by id, access");
		return new Department();
	}

}
