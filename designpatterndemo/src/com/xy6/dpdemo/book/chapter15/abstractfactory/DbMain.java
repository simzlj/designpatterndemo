package com.xy6.dpdemo.book.chapter15.abstractfactory;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.IDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.IUserService;

/**
 * 设计一个根据编号查询用户、部门的程序
 * <p>支持sqlserver、access作为db
 * 
 * <pre>
 * 创建用户服务接口，定义操作用户的方法，创建基于sqlservice的用户服务类，基于access的用户服务类，实现接口的方法。
 * 创建抽象工厂类，定义生产不同抽象服务的方法，创建具体工厂类，实现生产不同抽象服务的方法。
 * 
 * 抽象工厂模式。提供一个创建一系列对象的接口，而无需指定它们具体的类。
 * 抽象工厂，生产的是抽象的接口。
 * 工厂方法，生产的是具体的服务类。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-22
 */
public class DbMain {

	public static void main(String[] args) {
		test1();
		test2();
		test3();
	}
	
	/**
	 * 抽象工厂
	 */
	private static void test1(){
		IFactory factory = new SqlserverFactory();
		
		IUserService userService = factory.createUserService();
		userService.find("001");
		
		IDeptService deptService = factory.createDeptService();
		deptService.find("001");
	}
	
	/**
	 * 简单工厂
	 */
	private static void test2(){
		IUserService userService = DataAccess.getUserService();
		userService.find("001");
		
		IDeptService deptService = DataAccess.getDeptService();
		deptService.find("001");
	}
	
	/**
	 * 简单工厂+反射
	 */
	private static void test3(){
		IUserService userService = DataAccess.getUserServiceByReflect();
		userService.find("001");
		
		IDeptService deptService = DataAccess.getDeptServiceByReflect();
		deptService.find("001");
	}

}
