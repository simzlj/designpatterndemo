package com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.Department;

/**
 * 部门接口
 * <p>定义部门服务类具有的行为
 * 
 * @author zhang
 * @since 2017-06-22
 */
public interface IDeptService {

	public Department find(String id);
	
}
