package com.xy6.dpdemo.book.chapter15.abstractfactory;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.AccessDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.IDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.SqlserverDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.AccessUserService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.IUserService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.SqlserverUserService;

public class DataAccess {
	
	private final static String DB_SQL_SERVER = "Sqlserver"; 
	
	private final static String DB_ACCESS = "Access"; 

	private static String db = DB_SQL_SERVER;
	
	/**
	 * 获取用户服务类
	 * 
	 * @return
	 */
	public static IUserService getUserService(){
		switch(db){
			case DB_SQL_SERVER:
				return new SqlserverUserService();
			case DB_ACCESS:
				return new AccessUserService();
			default:
				break;
		}
		return null;
	}
	
	/**
	 * 获取部门服务类
	 * 
	 * @return
	 */
	public static IDeptService getDeptService(){
		switch(db){
			case DB_SQL_SERVER:
				return new SqlserverDeptService();
			case DB_ACCESS:
				return new AccessDeptService();
			default:
				break;
		}
		return null;
	}
	
	/**
	 * 反射获取用户服务类
	 * 
	 * @return
	 */
	public static IUserService getUserServiceByReflect(){
		String className = "com.xy6.dpdemo.book.chapter15.changedb.user.service." + db + "UserService";
		@SuppressWarnings("rawtypes")
		Class clazz = null;
		try {
			clazz = Class.forName(className);
			IUserService userService = (IUserService) clazz.newInstance();
			return userService;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 反射获取部门服务类
	 * 
	 * @return
	 */
	public static IDeptService getDeptServiceByReflect(){
		String className = "com.xy6.dpdemo.book.chapter15.changedb.dept.service." + db + "DeptService";
		@SuppressWarnings("rawtypes")
		Class clazz = null;
		try {
			clazz = Class.forName(className);
			IDeptService deptService = (IDeptService) clazz.newInstance();
			return deptService;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
