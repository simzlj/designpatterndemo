package com.xy6.dpdemo.book.chapter15.abstractfactory;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.IDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.SqlserverDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.IUserService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.SqlserverUserService;

/**
 * sqlserver数据库工厂类
 * <p>生产以sqlserver作为db的服务类
 * 
 * @author zhang
 * @since 2017-06-22
 */
public class SqlserverFactory implements IFactory {

	@Override
	public IUserService createUserService() {
		return new SqlserverUserService();
	}

	@Override
	public IDeptService createDeptService() {
		return new SqlserverDeptService();
	}
	
}
