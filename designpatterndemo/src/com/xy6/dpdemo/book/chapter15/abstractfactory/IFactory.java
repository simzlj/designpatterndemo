package com.xy6.dpdemo.book.chapter15.abstractfactory;

import com.xy6.dpdemo.book.chapter15.abstractfactory.dept.service.IDeptService;
import com.xy6.dpdemo.book.chapter15.abstractfactory.user.service.IUserService;

/**
 * 工厂接口
 * <p>生产指定类型的抽象产品
 * 
 * @author zhang
 * @since 2017-06-22
 */
public interface IFactory {

	public IUserService createUserService();
	
	public IDeptService createDeptService();
	
}
