package com.xy6.dpdemo.book.chapter24.chain;

import java.util.HashMap;
import java.util.Map;

/**
 * 职位类型枚举
 * 
 * @author zhang
 * @since 2017-07-10
 */
public enum ApproverEnum {

	GROUP_LEADER("GROPU_LEADER", "组长"),
	
	PROJECT_MANAGER("GROPU_LEADER", "项目经理"),
	
	DEPT_MANAGER("DEPT_MANAGER", "部门经理"),
	
	BOSS("BOSS", "老板");
	
	private String type;
	
	private String msg;
	
	private static Map<String, ApproverEnum> map;
	
	private ApproverEnum(String type, String msg){
		this.type = type;
		this.msg = msg;
		initMap(type, this);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	private static void initMap(String type, ApproverEnum bean){
		if(null == map){
			map = new HashMap<>();
		}
		map.put(type, bean);
	}
	
	public static ApproverEnum get(String type){
		if(null == map){
			return null;
		}
		return map.get(type);
	}
	
}
