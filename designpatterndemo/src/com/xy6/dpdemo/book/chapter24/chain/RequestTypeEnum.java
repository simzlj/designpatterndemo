package com.xy6.dpdemo.book.chapter24.chain;

import java.util.HashMap;
import java.util.Map;

/**
 * 申请单类型
 * 
 * @author zhang
 * @since 2017-07-10
 */
public enum RequestTypeEnum {

	SALARY("SALARY", "加薪"),
	
	ILL("ILL", "生病去医院"),
	
	HOLIDAY("HOLIDAY", "请假");
	
	private String type;
	
	private String msg;
	
	private static Map<String, RequestTypeEnum> map;
	
	private RequestTypeEnum(String type, String msg){
		this.type = type;
		this.msg = msg;
		put(type, this);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	private static void put(String type, RequestTypeEnum bean){
		if(null == map){
			map = new HashMap<>();
		}
		map.put(type, bean);
	}
	
	public static RequestTypeEnum get(String type){
		if(null == map){
			return null;
		}
		return map.get(type);
	}
	
}
