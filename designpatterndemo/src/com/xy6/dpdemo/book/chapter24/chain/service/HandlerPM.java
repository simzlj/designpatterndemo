package com.xy6.dpdemo.book.chapter24.chain.service;

import com.xy6.dpdemo.book.chapter24.chain.Request;
import com.xy6.dpdemo.book.chapter24.chain.RequestTypeEnum;

/**
 * 项目经理处理类
 * 
 * @author zhang
 * @since 2017-07-10
 */
public class HandlerPM extends Handler {

	@Override
	public void handle(Request req) {
		if (RequestTypeEnum.HOLIDAY.getType().equals(req.getType()) && req.getNum() <= 5) {
			System.out.println("审批通过，" + req.toString());
		} else if(super.successor != null){
			super.successor.handle(req);
		}
	}

}
