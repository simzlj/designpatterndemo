package com.xy6.dpdemo.book.chapter24.chain.service;

import com.xy6.dpdemo.book.chapter24.chain.Request;
import com.xy6.dpdemo.book.chapter24.chain.RequestTypeEnum;

/**
 * 部门经理处理类
 * 
 * @author zhang
 * @since 2017-07-10
 */
public class HandlerDM extends Handler {

	@Override
	public void handle(Request req) {
		if(RequestTypeEnum.HOLIDAY.getType().equals(req.getType())){
			System.out.println("审批通过，" + req.toString());
		} else if(RequestTypeEnum.SALARY.getType().equals(req.getType())){
			System.out.println("待定，" + req.toString());
		} else if(super.successor != null){
			super.successor.handle(req);
		}
	}

}
