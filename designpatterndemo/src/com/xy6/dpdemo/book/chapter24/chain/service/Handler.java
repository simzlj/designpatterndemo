package com.xy6.dpdemo.book.chapter24.chain.service;

import com.xy6.dpdemo.book.chapter24.chain.Request;

/**
 * 处理接口
 * <p>设置下一个处理人，定义处理方法
 * 
 * @author zhang
 * @since 2017-07-10
 */
public abstract class Handler {

	protected Handler successor;
	
	public void setSuccessor(Handler handler){
		this.successor = handler;
	}

	public abstract void handle(Request req);
	
}
