package com.xy6.dpdemo.book.chapter24.chain;

/**
 * 申请bean
 * <p>申请加薪或者请假
 * 
 * @author zhang
 * @since 2017-07-10
 */
public class Request {

	private String type;
	
	private Integer num;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Override
	public String toString() {
		return "Request [type=" + type + ", num=" + num + "]";
	}
	
}
