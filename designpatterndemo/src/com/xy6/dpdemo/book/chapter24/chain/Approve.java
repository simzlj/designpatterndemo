package com.xy6.dpdemo.book.chapter24.chain;

/**
 * 审批
 * 
 * @author zhang
 * @since 2017-07-10
 */
public class Approve {

	/**
	 * 审批人
	 */
	private String name;
	
	public Approve(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 审批一个申请
	 * <p>先判断当前人身份，再判断申请单类型
	 * 
	 * @param req
	 */
	public void approve(Request req){
		if(ApproverEnum.GROUP_LEADER.getType().equals(this.name)){
			if(RequestTypeEnum.ILL.getType().equals(req.getType())){
				System.out.println("审批通过，" + req.toString());
			} else if(RequestTypeEnum.HOLIDAY.getType().equals(req.getType()) && req.getNum() <= 2){
				System.out.println("审批通过，" + req.toString());
			} else {
				System.out.println("驳回，无权审批，" + req.toString());
			}
		} else if(ApproverEnum.PROJECT_MANAGER.getType().equals(this.name)){
			if(RequestTypeEnum.HOLIDAY.getType().equals(req.getType()) && req.getNum() <= 5){
				System.out.println("审批通过，" + req.toString());
			} else {
				System.out.println("驳回，无权审批，" + req.toString());
			}
		} else if(ApproverEnum.DEPT_MANAGER.getType().equals(this.name)){
			if(RequestTypeEnum.HOLIDAY.getType().equals(req.getType())){
				System.out.println("审批通过，" + req.toString());
			} else if(RequestTypeEnum.SALARY.getType().equals(req.getType()) && req.getNum() <= 10000){
				System.out.println("审批通过，" + req.toString());
			} else {
				System.out.println("驳回，无权审批，" + req.toString());
			}
		} else {
			System.out.println("待定，" + req.toString());
		}
	}
	
}
