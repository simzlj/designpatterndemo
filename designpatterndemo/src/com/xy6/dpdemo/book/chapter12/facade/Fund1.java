package com.xy6.dpdemo.book.chapter12.facade;

/**
 * 基金bean
 * <p>可以买入/卖出各种股票、债券等资产
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class Fund1 extends Exchange {

	private Stock1 stock1;
	
	private Stock2 stock2;
	
	private Bond1 bond1;
	
	public Fund1(){
		stock1 = new Stock1();
		stock2 = new Stock2();
		bond1 = new Bond1();
	}
	
	@Override
	public void buy() {
		stock1.buy();
		stock2.buy();
		bond1.buy();
	}

	@Override
	public void sell() {
		stock1.sell();
		stock2.sell();
		bond1.sell();
	}

}
