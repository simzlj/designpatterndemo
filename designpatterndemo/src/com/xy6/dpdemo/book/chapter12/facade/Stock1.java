package com.xy6.dpdemo.book.chapter12.facade;

/**
 * 股票bean
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class Stock1 extends Exchange {
	
	@Override
	public void buy(){
		System.out.println("buy stock1");
	}
	
	@Override
	public void sell(){
		System.out.println("sell stock1");
	}

}
