package com.xy6.dpdemo.book.chapter12.facade;

/**
 * 设计一个炒股的程序，可以买股票、债券等各种证券
 * 
 * <pre>
 * 逐个买入股票、债券等证券。麻烦，客户端依赖的信息叫多，需要了解所有的证券信息。
 * 改：买入基金。由基金去买各种股票、债券。客户端只依赖基金。
 * 
 * 外观模式，为子系统中的一组接口提供一个一致的界面。此模式定义了一个高层接口，这个接口使得这一子系统更加易于使用。
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-19
 */
public class StockMain {

	public static void main(String[] args) {
		Exchange fund1 = new Fund1();
		// 申购
		fund1.buy();
		// 赎回
		fund1.sell();
	}

}
