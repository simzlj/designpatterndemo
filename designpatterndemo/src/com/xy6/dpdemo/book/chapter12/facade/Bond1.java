package com.xy6.dpdemo.book.chapter12.facade;

public class Bond1 extends Exchange {
	
	@Override
	public void buy(){
		System.out.println("buy bond1");
	}
	
	@Override
	public void sell(){
		System.out.println("sell bond1");
	}

}
