package com.xy6.dpdemo.book.chapter12.facade;

/**
 * 证券交易接口
 * 
 * @author zhang
 * @since 2017-06-19
 */
public abstract class Exchange {

	public abstract void buy();
	
	public abstract void sell();
	
}
