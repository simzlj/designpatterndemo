package com.xy6.dpdemo.book.chapter07.proxy.gift;

/**
 * 女孩bean
 * 
 * @author zhang
 * @since 2017-06-14
 */
public class SchoolGirl {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
