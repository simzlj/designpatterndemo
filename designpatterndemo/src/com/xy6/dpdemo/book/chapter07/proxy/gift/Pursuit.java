package com.xy6.dpdemo.book.chapter07.proxy.gift;

/**
 * 追求者
 * <p>实现送各种礼物接口
 * 
 * @author zhang
 * @since 2017-06-14
 */
public class Pursuit implements GiveGiftInterface {

	private SchoolGirl girl;
	
	public Pursuit(SchoolGirl girl){
		this.girl = girl;
	}
	
	@Override
	public void giveDoll() {
		System.out.println(girl.getName() + ", give u a doll");
	}

	@Override
	public void giveFlower() {
		System.out.println(girl.getName() + ", give u a flower");
	}

	@Override
	public void giveChocolate() {
		System.out.println(girl.getName() + ", give u a chocolate");
	}

}
