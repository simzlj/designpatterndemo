package com.xy6.dpdemo.book.chapter07.proxy.image;

/**
 * 设计一个从显示图片程序
 * <p>首次从磁盘加载图片，后续直接从内存读取
 * 
 * <pre>
 * 设计演变过程：
 * 新建一个类，根据指定文件路径，从磁盘加载图片，显示出来。
 * 新建一个静态变量，保存已加载的图片，再次访问时，直接读取。
 * 改：新建代理类，在创建图片对象与加载图片中间加一层，隐藏加载图片实现。代理模式
 * 
 * Q：上述两种方案，对于客户端来说似乎差异不大：都是一个类，入参为文件全路径。为什么第二种方案更好？
 * 
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-15
 */
public class ImageMain {

	public static void main(String[] args) {
		String fileName = "D://1.png";
		ProxyImage proxy = new ProxyImage(fileName);
		proxy.display();
		
		proxy.display();
	}

}
