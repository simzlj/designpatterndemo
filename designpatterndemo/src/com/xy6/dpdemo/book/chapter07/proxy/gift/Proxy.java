package com.xy6.dpdemo.book.chapter07.proxy.gift;

/**
 * 代理类
 * <p>代替某个男孩，向某个女孩送礼物
 * 
 * @author zhang
 * @since 2017-06-14
 */
public class Proxy implements GiveGiftInterface {

	private Pursuit boy;
	
	public Proxy(SchoolGirl girl){
		boy = new Pursuit(girl);
	}
	
	@Override
	public void giveDoll() {
		boy.giveDoll();
	}

	@Override
	public void giveFlower() {
		boy.giveFlower();
	}

	@Override
	public void giveChocolate() {
		boy.giveChocolate();
	}

}
