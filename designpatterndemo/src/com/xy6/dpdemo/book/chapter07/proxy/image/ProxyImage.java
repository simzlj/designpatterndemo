package com.xy6.dpdemo.book.chapter07.proxy.image;

/**
 * 代理类
 * <p>代理图片类展示图片。对客户端开放。
 * 
 * @author zhang
 * @since 2017-06-15
 */
public class ProxyImage implements ImageInterface {

	private String fileName;
	private RealImage realImage;
	
	public ProxyImage(String fileName){
		this.fileName = fileName;
	}
	
	@Override
	public void display() {
		if(null == realImage){
			realImage = new RealImage(fileName);
		}
		realImage.display();
	}

}
