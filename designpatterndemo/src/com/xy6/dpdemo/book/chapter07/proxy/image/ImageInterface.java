package com.xy6.dpdemo.book.chapter07.proxy.image;

/**
 * 图片接口
 * <p>图片具体类、代理类都实现该接口，接口对客户端不可见
 * 
 * @author zhang
 * @since 2017-06-15
 */
public interface ImageInterface {

	public void display();
	
}
