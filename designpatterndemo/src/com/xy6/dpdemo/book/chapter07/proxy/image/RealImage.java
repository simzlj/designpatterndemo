package com.xy6.dpdemo.book.chapter07.proxy.image;

/**
 * 真实图片类
 * <p>被代理的对象，在该对象中实现从磁盘中加载图片。对客户端不可见。
 * 
 * @author zhang
 * @since 2017-06-15
 */
public class RealImage implements ImageInterface {

	private String fileName;
	
	public RealImage(String fileName){
		this.fileName = fileName;
		this.loadFromDisk(fileName);
	}
	
	@Override
	public void display() {
		System.out.println("displaying :" + fileName);
	}
	
	private void loadFromDisk(String fileName){
		System.out.println("loading :" + fileName);
	}

}
