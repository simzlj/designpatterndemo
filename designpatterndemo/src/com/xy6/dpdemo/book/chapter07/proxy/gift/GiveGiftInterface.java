package com.xy6.dpdemo.book.chapter07.proxy.gift;

/**
 * 送礼物接口
 * 
 * @author zhang
 * @since 2017-06-14
 */
public interface GiveGiftInterface {

	public void giveDoll();
	
	public void giveFlower();
	
	public void giveChocolate();
	
}
