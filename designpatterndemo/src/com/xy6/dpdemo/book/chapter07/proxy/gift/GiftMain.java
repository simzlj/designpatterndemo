package com.xy6.dpdemo.book.chapter07.proxy.gift;

/**
 * 设计一个送礼物程序，代替某个男孩向某个女孩送礼物
 * 
 * <pre>
 * 设计演变过程：
 * 创建一个类，指定男孩、女孩对象，提供送礼物方法。直接送礼物，而非代替某个男孩送礼物
 * 改：创建送礼物接口，创建追求者类、代理类。追求者类中，指定男孩、女孩对象，
 * 	代理类中，传入女孩对象，根据女孩对象创建追求者对象，调用追求者送礼物方法，实现送出礼物。代理模式
 * 
 * 应用场景：远程代理，如webservice；虚拟代理，如html加载图片；安全代理，如ACL授权；
 * </pre>
 * 
 * @author zhang
 * @since 2017-06-14
 */
public class GiftMain {

	public static void main(String[] args) {
		SchoolGirl girl = new SchoolGirl();
		girl.setName("Lily");
		
		Proxy daili = new Proxy(girl);
		daili.giveDoll();
		daili.giveFlower();
		daili.giveChocolate();
	}

}
